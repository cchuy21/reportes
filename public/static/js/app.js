'(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';
function getParent(ob, cl) {
	do {
		ob = ob.parent();
	} while (!ob.hasClass(cl));
	return ob;
}
var ed=0;
var arrayc=[];
function padre(element, clase) {
	var tr = false;
	while (!tr) {
		if (element.parent().hasClass(clase)) {
			tr = true;
			return element.parent();
			break;
		} else {
			element = element.parent();
		}
	}
}
/*manda los mensajes cuando se realiza una acción en un botón especifico*/
/*--------------------Objects-----------------------*/
var app = {
	'load' : function(url, container2, options, callback) {
		var container = jQuery(container2);

		if (jQuery.isFunction(options)) {
			callback = options;
		} else {
			if (jQuery.isFunction(options.done)) {
				callback = options.done;
			}
			if (jQuery.isFunction(options.before)) {
				options.before();
			}
		}
		jQuery.ajax({
			type : 'POST',
			dataType : 'html',
			url : url
		}).done(function(d) {
			container.html(d);
			callback();
		});
	}
};
//Funcion generica para autocomplete, todos deben tener el elemente enseguida
//Cambiar esto a un plugin para que se corrija
jQuery(document).ready(function(){
	initFormLogin();
});
function initFormLogin() {
	jQuery('.form-signin').fadeTo(1, 1);
		//inituser
		if (!jQuery(".form-signin").length) {
            //code
			inituser();
			//inituserpanel();
			//inituserbuttons();
        }
		
 	jQuery(".form-signin .btn-block").on("click",function(){
		//obtiene el valor del nombre de usuario que escribieron
		var nm = jQuery("input[name='user[username]']").val();
		var pass = jQuery("input[name='user[password]']").val();
		//manda los datos por el metodo post(ocultos)
		jQuery.ajax({
			url : "/systemuser/login/",
			data : {
				user:nm,
				pass:pass
				},
			type : "POST",
			dataType : "json",
			success : function(source) {				
					//te manda a la página principal del sitio
					window.location = "/index.php";
				}
			
		});

	});
	jQuery('.form-signin').fadeIn(function() {
		//manda el mensaje en consola del navegador
		console.log("Formulario de inicio de sesión cargado.");
	});
}
function inituser(){
	jQuery('#tabsa > li > a').click(function (e) {
		console.log(e);
		e.preventDefault();
		jQuery(this).tab('show');
	});
	jQuery(".btn-personal").off("click").on("click",function(){
		var mespersonal = jQuery(".datepersonalm").val();
		var anopersonal = jQuery(".datepersonala").val();
		app.load("/personal/plantilla/" + mespersonal + "/" + anopersonal,".contenedorpersonal",function(){
			personal(mespersonal,anopersonal);
		});
	});
	jQuery(".btn-personalr").off("click").on("click",function(){
		var mespersonal = jQuery(".datepersonalm").val();
		var anopersonal = jQuery(".datepersonala").val();
		app.load("/personal/plantilla/" + mespersonal + "/" + anopersonal,".contenedorpersonalr",function(){
			
		});
	});
	jQuery(".datetiempo").off("change").on("change",function(){
		var fec = jQuery(this).val();
		jQuery(".mesano").find("h3").html("Año: " + fec);
		app.load("/extra/plantilla/" + fec,".contenedor",function(){
			jQuery(".btn-guardar").off("click").on("click", function(){
				var arr1 = {};
				var arr2 = {};
				var arr3 = {};
				jQuery(".adm").each(function(){
					arr1[jQuery(this).attr("name")]=jQuery(this).val();
				});
				jQuery(".ope").each(function(){
					arr2[jQuery(this).attr("name")]=jQuery(this).val();
				});
				jQuery(".opex").each(function(){
					arr3[jQuery(this).attr("name")]=jQuery(this).val();
				});
				arr1 = JSON.stringify(arr1);
				arr2 = JSON.stringify(arr2);
				arr3 = JSON.stringify(arr3);
				jQuery.ajax({
					url : "/extra/guardar",
					data : {
						an: fec,
						adm:arr1,
						ope:arr2,
						opex:arr3,
					},
					type : "POST",
					dataType : "json",
					success : function(source) {
						console.log(source);
					}
				});
			});
		});
	});
	jQuery(".daterep1").off("change").on("change",function(){
		var fec = jQuery(this).val();
		app.load("/reportes/extraplantilla/" + fec,".contenedorrep1",function(){
		});
	});
	jQuery(".datemth").off("change").on("change",function(){
		var fec = jQuery(this).val();
		app.load("/reportes/mthplantilla/" + fec,".contenedormth",function(){
			jQuery(".contenedormth").find('input').prop('disabled',true);
		});
	});
	jQuery(".dateclv").off("change").on("change",function(){
		var fec = jQuery(this).val();
		app.load("/reportes/clvplantilla/" + fec,".contenedorclv",function(){
			jQuery(".contenedorclv").find('input').prop('disabled',true);
		});
	});
	jQuery(".datefaltas").off("change").on("change",function(){
		var fec = jQuery(this).val();
		jQuery(".mesanofaltas").find("h3").html("Año: " + fec);
		app.load("/faltas/plantilla/" + fec,".contenedorfaltas",function(){
			initfaltas(".iniciomes",["adm","indi","dir"],".iniciodemestotal", sumastt, function(){}, true);
			initfaltas(".tadmcontrato",["planta","prueba"],".tadmcontratosubtotal", function(){}, subsum, true);
			initfaltas(".topecontrato",["planta","prueba"],".topecontratosubtotal", function(){}, subsum, true);
			initfaltas(".sexoadm",["planta","prueba"],".sexoadmsubtotal", function(){},subsum, true);
			initfaltas(".sexoope",["planta","prueba"],".sexoopesubtotal", function(){},subsum, true);
			initfaltas(".altas",["adm","ope"],".altastotal", sumastt, function(){}, 0);
			initfaltas(".bajasadm",["planta","prueba"],".bajasadmsubtotal", sumastt, subsum, 0);
			initfaltas(".bajasope",["planta","prueba"],".bajasopesubtotal", sumastt, subsum, 0);
			initfaltas(".peradm",["inc","vac","fal"],".peradmsubtotal", function(){}, subsum, 0);
			initfaltas(".perope",["inc","vac","fal"],".peropesubtotal", function(){}, subsum, 0);
			jQuery(".gbal").find('input').prop('disabled',true);
			jQuery(".diaslab").find("input").off("click").on("click", function(){
				var m = jQuery(this).data("mes");
				final(m);
			});
			jQuery(".btn-guardar").off("click").on("click", function(){
				var arr1 = [];
				jQuery("input").each(function(){
					arr1.push(jQuery(this).val());
				});
				arr1 = JSON.stringify(arr1);
				jQuery.ajax({
					url : "/faltas/guardar",
					data : {
						an: fec,
						valores:arr1,
					},
					type : "POST",
					dataType : "json",
					success : function(source) {
						console.log(source);
					}
				});
			});
			jQuery.ajax({
				url : "/faltas/get",
				data : {
					an: fec
				},
				type : "POST",
				dataType : "json",
				success : function(source) {
					var cont=0;
					jQuery("input").each(function(){
						jQuery(this).val(source[cont]);
						if(source[cont]!="")
						jQuery(this).trigger('change');
						cont++;
					});
				}
			});
		});
	});
}
function subsum(id){
	var s1 = parseInt(jQuery(".tadmcontratosubtotal").find(".inp"+id).html());
	var s2 = parseInt(jQuery(".topecontratosubtotal").find(".inp"+id).html());
	if(Number.isNaN(s1)){
		s1 = 0;
	}
	if(Number.isNaN(s2)){
		s2 = 0;
	}
	var sumt = s1 + s2;
	jQuery(".topecontratostotal").find(".inp"+id).html(sumt);
	var sx1 = parseInt(jQuery(".sexoadmsubtotal").find(".inp"+id).html());
	var sx2 = parseInt(jQuery(".sexoopesubtotal").find(".inp"+id).html());
	if(Number.isNaN(sx1)){
		sx1 = 0;
	}
	if(Number.isNaN(sx2)){
		sx2 = 0;
	}
	var sxumt = sx1 + sx2;
	jQuery(".sexototal").find(".inp"+id).html(sxumt);

	var sb1 = parseInt(jQuery(".bajasadmsubtotal").find(".inp"+id).html());
	var sb2 = parseInt(jQuery(".bajasopesubtotal").find(".inp"+id).html());
	if(Number.isNaN(sb1)){
		sb1 = 0;
	}
	if(Number.isNaN(sb2)){
		sb2 = 0;
	}
	var sbumt = sb1 + sb2;
	jQuery(".bajastotal").find(".inp"+id).html(sbumt);
	var sp1 = parseInt(jQuery(".peradmsubtotal").find(".inp"+id).html());
	var sp2 = parseInt(jQuery(".peropesubtotal").find(".inp"+id).html());
	if(Number.isNaN(sp1)){
		sp1 = 0;
	}
	if(Number.isNaN(sp2)){
		sp2 = 0;
	}
	var spumt = sp1 + sp2;
	jQuery(".pertotal").find(".inp"+id).html(spumt);
	final(id);
}
function sumastt(id){
	var v1 = parseInt(jQuery(".iniciomesadm").find(".mes"+id).val());
	var v2 = parseInt(jQuery(".iniciomesindi").find(".mes"+id).val());
	var v3 = parseInt(jQuery(".altasadm").find(".mes"+id).val());
	var v4 = parseInt(jQuery(".bajasadmsubtotal").find(".inp"+id).html());
	if(Number.isNaN(v1)){
		v1 = 0;
	}
	if(Number.isNaN(v2)){
		v2 = 0;
	}
	if(Number.isNaN(v3)){
		v3 = 0;
	}
	if(Number.isNaN(v4)){
		v4 = 0;
	}
	var sm = v1+v2+v3-v4;
	jQuery(".finadm").find(".inp"+id).html(sm);

	var v1o = parseInt(jQuery(".iniciomesindi").find(".mes"+id).val());
	var v2o = parseInt(jQuery(".iniciomesdir").find(".mes"+id).val());
	var v3o = parseInt(jQuery(".altasope").find(".mes"+id).val());
	var v4o = parseInt(jQuery(".bajasopesubtotal").find(".inp"+id).html());
	if(Number.isNaN(v1o)){
		v1o = 0;
	}
	if(Number.isNaN(v2o)){
		v2o = 0;
	}
	if(Number.isNaN(v3o)){
		v3o = 0;
	}
	if(Number.isNaN(v4o)){
		v4o = 0;
	}
	var smo = v1o+v2o+v3o-v4o;
	jQuery(".finope").find(".inp"+id).html(smo);
	jQuery(".findemestotal").find(".inp"+id).html((smo+sm));
	final(id);
}
function final(id){
	setTimeout(function() {
		var vf1 = parseInt(jQuery(".findemestotal").find(".inp"+id).html());
		var vf2 = parseInt(jQuery(".bajastotal").find(".inp"+id).html());
		if(Number.isNaN(vf1)){
			vf1 = 0;
		}
		if(Number.isNaN(vf2)){
			vf2 = 0;
		}
		var vff = (vf2/(vf1+vf2)).toFixed(2);
		if(!isNaN(vff))
		jQuery(".ausrot").find(".inp"+id).html(vff);
		var vf1 = parseInt(jQuery(".pertotal").find(".inp"+id).html());
		var vf2 = parseInt(jQuery(".findemestotal").find(".inp"+id).html());
		var vf3 = parseInt(jQuery(".bajastotal").find(".inp"+id).html());
		var vf4 = parseInt(jQuery(".diaslab").find(".mes"+id).val());
		var vff = (vf1/((vf2+vf3)*vf4)).toFixed(2);
		console.log(vf1+" "+vf2+" "+vf3+" "+vf4);
		console.log(vff);
		if(!isNaN(vff))
		jQuery(".rot").find(".inp"+id).html(vff);
	}, 2000);
}
function initfaltas(busq,comp, tot, sum, sum2, por){
	//jQuery(busq+comp[0]).find(".mes1").val(123);
	for(com in comp){
		for(var i = 1; i<=12; i++){
			jQuery(busq+comp[com]).find(".mes"+i).off("change").on("change", function(){
				var t = 0;
				var m = jQuery(this).data("mes");
				var cl = jQuery(this).attr("class");
				cl = cl.split(" ");

				for(coma in comp){
					if(jQuery(busq+comp[coma]).find("."+cl[1]).val() == ""){
					}else{
						t += parseInt(jQuery(busq+comp[coma]).find("."+cl[1]).val());
					}
				}
				for(coma in comp){
					var vol = ((parseInt(jQuery(busq+comp[coma]).find(".mes"+ m).val())*100/t).toFixed(2));
					if(!isNaN(vol))
					jQuery(busq+comp[coma]).find(".por" + m).html(vol);
				}
				if(!isNaN(t))
				jQuery(tot).find(".inp"+ m).html(t);
				if(por){
					var vol2 = ((t*100/t).toFixed(2));
					if(!isNaN(vol2))
					jQuery(tot).find(".por" + m).html(vol2);
				}
				sum(m);
				sum2(m);
			});
		}
	}
}
function personal(me,an){
	jQuery.ajax({
		url : "/personal/get",
		data : {
			an: an,
			mes:me
		},
		type : "POST",
		dataType : "json",
		success : function(source) {
			var cont=0;
			jQuery("input").each(function(){
				jQuery(this).val(source[cont]);
				cont++;
			});
			jQuery("#table1").find('input:first').trigger('change');
			jQuery("#table2").find('input:first').trigger('change');
			jQuery("#table3").find('input:first').trigger('change');
			jQuery("#table4").find('input:first').trigger('change');
			jQuery("#table5").find('input:first').trigger('change');
			jQuery("#table6").find('input:first').trigger('change');
		}
	});
	jQuery("#table3").find("input").on("change",function(){
		var tab3=[];	
		jQuery("#table3").find("input").each(function(){
			var de = parseFloat(jQuery(this).val());
			if(!isNaN(de)){
				tab3.push(de);
			}else{
				tab3.push(0);
			}
		});
		var r1 = tab3[0]+tab3[1]+tab3[2]+tab3[3]+tab3[4]+tab3[5]+tab3[6]+tab3[7]+tab3[8]+tab3[9]+tab3[10]+tab3[11];
		var r2 = tab3[13]+tab3[14];
		var r3 = tab3[15]+tab3[16]+tab3[17]+tab3[18]+tab3[19];
		var r4 = r1+r2+r3+tab3[20]+tab3[12];
		var r5 = r4-tab3[19]-tab3[16]-tab3[17];
		jQuery(".ttl1").html((r1.toFixed(2)));
		jQuery(".ttl2").html((r2.toFixed(2)));
		jQuery(".ttl3").html((r3.toFixed(2)));
		jQuery(".ttl4").html((r4.toFixed(2)));
		jQuery(".ttl5").html((r5.toFixed(2)));
	});
	jQuery("#table6").find("input").on("change",function(){
		var tab6=[];	
		jQuery("#table6").find("input").each(function(){
			var de = parseFloat(jQuery(this).val());
			if(!isNaN(de)){
				tab6.push(de);
			}else{
				tab6.push(0);
			}
		});
		var r6 = tab6[5]+tab6[6];
		var r7 = tab6[8]+tab6[9]+tab6[10]+tab6[11]+tab6[12];
		var r8 = tab6[0]+tab6[1]+tab6[2]+tab6[3]+tab6[4]+tab6[7]+r7+tab6[13]+r6;
		var r9 = tab6[14]+tab6[15]+tab6[16]+tab6[17]+tab6[18];
		jQuery(".ttl6").html((r6.toFixed(2)));
		jQuery(".ttl7").html((r7.toFixed(2)));
		jQuery(".ttl8").html((r8.toFixed(2)));
		jQuery(".ttl9").html((r9.toFixed(2)));
	});
	jQuery("#table1").find("input").on("change",function(){
		var tab1=[];
		var suba1=[];
		var ctb1=1;
		var ct = 1;
		jQuery("#table1").find("input").each(function(){
			var de = parseFloat(jQuery(this).val());
			if(ctb1==1){
				suba1 = [];
			}
			if(!isNaN(de)){
				suba1.push(de);
			}else{
				suba1.push(0);
			}
			
			ctb1++;
			if(ctb1==7){
				console.log(ct);
				if(ct==4 || ct==7 || ct==10 || ct==15){
					tab1.push([0,0,0,0,0,0]);
					ct++;
				}
				tab1.push(suba1);
				ctb1=1;
				ct++;
			}
		});
		tab1.push([0,0,0,0,0,0]);
		tab1.push([0,0,0,0,0,0]);
		for(var i = 0; i <= 5; i++){
			tab1[3][i]=tab1[1][i]+tab1[2][i];
			tab1[6][i]=tab1[4][i]+tab1[5][i];
			tab1[9][i]=tab1[7][i]+tab1[8][i];
			tab1[14][i]=tab1[3][i]+tab1[6][i]+tab1[9][i]+tab1[10][i]+tab1[11][i]+tab1[12][i]+tab1[13][i];
			tab1[18][i]=tab1[16][i]+tab1[17][i];
			tab1[19][i]=tab1[0][i]+tab1[14][i]+tab1[15][i]+tab1[18][i];
			
			jQuery(".tab13"+i).html(tab1[3][i]);
			jQuery(".tab16"+i).html(tab1[6][i]);
			jQuery(".tab19"+i).html(tab1[9][i]);
			jQuery(".tab114"+i).html(tab1[14][i]);
			jQuery(".tab118"+i).html(tab1[18][i]);
			jQuery(".tab119"+i).html(tab1[19][i]);
		}
		for(var tb1 in tab1){
			tab1[tb1][6] = 0;
			for(var i = 0; i <= 5; i++){
				tab1[tb1][6]+=tab1[tb1][i];
				jQuery(".tab1"+tb1+"6").html(tab1[tb1][6]);
			}
		}
		console.log(tab1);
	});
	jQuery("#table2").find("input").on("change",function(){

		var tab2=[];
		var suba2=[];
		var ctb2=1;
		var ct = 1;
		jQuery("#table2").find("input").each(function(){
			var de = parseFloat(jQuery(this).val());
			if(ctb2==1){
				suba2 = [];
			}
			if(!isNaN(de)){
				suba2.push(de);
			}else{
				suba2.push(0);
			}
			
			ctb2++;
			if(ctb2==7){
				console.log(ct);
				if(ct==4 || ct==7 || ct==10 || ct==15){
					tab2.push([0,0,0,0,0,0]);
					ct++;
				}
				tab2.push(suba2);
				ctb2=1;
				ct++;
			}
		});
		tab2.push([0,0,0,0,0,0]);
		tab2.push([0,0,0,0,0,0]);
		for(var i = 0; i <= 5; i++){
			tab2[3][i]=tab2[1][i]+tab2[2][i];
			tab2[6][i]=tab2[4][i]+tab2[5][i];
			tab2[9][i]=tab2[7][i]+tab2[8][i];
			tab2[14][i]=tab2[3][i]+tab2[6][i]+tab2[9][i]+tab2[10][i]+tab2[11][i]+tab2[12][i]+tab2[13][i];
			tab2[18][i]=tab2[16][i]+tab2[17][i];
			tab2[19][i]=tab2[0][i]+tab2[14][i]+tab2[15][i]+tab2[18][i];
			
			jQuery(".tab23"+i).html(tab2[3][i]);
			jQuery(".tab26"+i).html(tab2[6][i]);
			jQuery(".tab29"+i).html(tab2[9][i]);
			jQuery(".tab214"+i).html(tab2[14][i]);
			jQuery(".tab218"+i).html(tab2[18][i]);
			jQuery(".tab219"+i).html(tab2[19][i]);
		}
		for(var tb2 in tab2){
			tab2[tb2][6] = 0;
			for(var i = 0; i <= 5; i++){
				tab2[tb2][6]+=tab2[tb2][i];
				jQuery(".tab2"+tb2+"6").html(tab2[tb2][6]);
			}
		}
		console.log(tab2);
	});
	jQuery("#table4").find("input").on("change",function(){

		var tab4=[];
		var suba4=[];
		var ctb4=1;
		var ct = 1;
		var con = 0;
		jQuery("#table4").find("input").each(function(){
			con++;
			var de = parseFloat(jQuery(this).val());
			if(ctb4==1){
				suba4 = [];
			}
			if(!isNaN(de)){
				suba4.push(de);
			}else{
				suba4.push(0);
			}
			if(con == 3){
				suba4.push(0);			
			}
			if(con == 6){
				suba4.push(0);
				suba4.push(0);
				con=0;
			}
			ctb4++;
			if(ctb4==7){
				console.log(ct);
				if(ct==4 || ct==7 || ct==10 || ct==15 || ct==19 || ct==20){
					tab4.push([0,0,0,0,0,0,0,0,0]);
					ct++;
				}
				tab4.push(suba4);
				ctb4=1;
				ct++;
			}
		});
		
		tab4.push([0,0,0,0,0,0,0,0,0]);
		tab4.push([0,0,0,0,0,0,0,0,0]);
		
		for(var i = 0; i <= 8; i++){
			tab4[3][i]=tab4[1][i]+tab4[2][i];
			tab4[6][i]=tab4[4][i]+tab4[5][i];
			tab4[9][i]=tab4[7][i]+tab4[8][i];
			tab4[14][i]=tab4[3][i]+tab4[6][i]+tab4[9][i]+tab4[10][i]+tab4[11][i]+tab4[12][i]+tab4[13][i];
			tab4[18][i]=tab4[16][i]+tab4[17][i];
			tab4[19][i]=tab4[0][i]+tab4[14][i]+tab4[15][i]+tab4[18][i];
			
			jQuery(".tab43"+i).html(tab4[3][i]);
			jQuery(".tab46"+i).html(tab4[6][i]);
			jQuery(".tab49"+i).html(tab4[9][i]);
			jQuery(".tab414"+i).html(tab4[14][i]);
			jQuery(".tab418"+i).html(tab4[18][i]);
			jQuery(".tab419"+i).html(tab4[19][i]);
		}
		for(var tb4 in tab4){
			if(tb4 < 20){
				tab4[tb4][3] = 0;
				tab4[tb4][7] = 0;
				tab4[tb4][8] = 0;
				tab4[tb4][3]=tab4[tb4][0]-tab4[tb4][1]+tab4[tb4][2];
				jQuery(".tab4"+tb4+"3").html(tab4[tb4][3]);
				
				tab4[tb4][7]=tab4[tb4][4]-tab4[tb4][5]+tab4[tb4][6];
				jQuery(".tab4"+tb4+"7").html(tab4[tb4][7]);
	
				tab4[tb4][8]=tab4[tb4][3]+tab4[tb4][7];
				jQuery(".tab4"+tb4+"8").html(tab4[tb4][8]);
			}
		}
		var h1 = parseFloat(jQuery(".tab4200").val());
		if(isNaN(h1)){
			h1 = 0;
		}
		var h2 = parseFloat(jQuery(".tab4201").val());
		if(isNaN(h2)){
			h2 = 0;
		}
		var hom = h1+h2;
		var m1 = parseFloat(jQuery(".tab4210").val());
		if(isNaN(m1)){
			m1 = 0;
		}
		var m2 = parseFloat(jQuery(".tab4211").val());
		if(isNaN(m2)){
			m2 = 0;
		}
		var muj = m1+m2;
		jQuery(".tab4202").html(hom);
		jQuery(".tab4212").html(muj);
		console.log(tab4);
	});
	jQuery("#table5").find("input").on("change",function(){

		var tab5=[];
		var suba5=[];
		var ctb5=1;
		var ct = 1;
		var con5 = 0;

		jQuery("#table5").find("input").each(function(){
			con5++;
			var de = parseFloat(jQuery(this).val());
			if(ctb5==1){
				suba5 = [];
			}
			if(!isNaN(de)){
				suba5.push(de);
			}else{
				suba5.push(0);
			}
						
			
			ctb5++;
			if(ctb5==3){
				console.log(ct);
				if(ct==4 || ct==7 || ct==10 || ct==15 || ct==19 || ct==20){
					tab5.push([0,0,0]);
					ct++;
				}
				suba5.push(0);	
				tab5.push(suba5);
				ctb5=1;
				ct++;
			}
		});
		tab5.push([0,0,0]);
		tab5.push([0,0,0]);
		for(var i = 0; i <= 1; i++){
			tab5[3][i]=tab5[1][i]+tab5[2][i];
			tab5[6][i]=tab5[4][i]+tab5[5][i];
			tab5[9][i]=tab5[7][i]+tab5[8][i];
			tab5[14][i]=tab5[10][i]+tab5[11][i]+tab5[12][i]+tab5[13][i];
			tab5[18][i]=tab5[16][i]+tab5[17][i];
			tab5[19][i]=tab5[0][i]+tab5[3][i]+tab5[6][i]+tab5[9][i]+tab5[14][i]+tab5[15][i]+tab5[18][i];
			
			jQuery(".tab53"+i).html(tab5[3][i].toFixed(2));
			jQuery(".tab56"+i).html(tab5[6][i].toFixed(2));
			jQuery(".tab59"+i).html(tab5[9][i].toFixed(2));
			jQuery(".tab514"+i).html(tab5[14][i].toFixed(2));
			jQuery(".tab518"+i).html(tab5[18][i].toFixed(2));
			jQuery(".tab519"+i).html(tab5[19][i].toFixed(2));
		}
		for(var tb5 in tab5){
			tab5[tb5][2] = 0;
			for(var i = 0; i <= 1; i++){
				tab5[tb5][2]+=tab5[tb5][i];
				jQuery(".tab5"+tb5+"2").html(tab5[tb5][2].toFixed(2));
			}
		}
		console.log(tab5);
	});
	jQuery(".btn-gpersonal").on("click", function(){
		var arr2 = [];
		jQuery("input").each(function(){
			arr2.push(jQuery(this).val());
		});
		arr2 = JSON.stringify(arr2);
		jQuery.ajax({
			url : "/personal/guardar",
			data : {
				an: an,
				mes: me,
				valores:arr2,
			},
			type : "POST",
			dataType : "json",
			success : function(source) {
				console.log(source);
			}
		});
	});
}