<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
error_reporting(1);

    header('Content-Type: text/html; charset=UTF-8');
	//ini_set('display_errors','On');
    //ini_set('session.save_handler', 'mysql');
    date_default_timezone_set('America/Monterrey');
    //mb_internal_encoding('UTF-8');
    //mb_http_output('UTF-8');
        
	include '../core/bootstrapper.php';  
        
	bootstrapper::init();
        
    $defController = configDriver::defaultController();

	$components = array();
	
	if(isset($_SERVER['ORIG_PATH_INFO'])) {
		$components = explode("/", substr($_SERVER['ORIG_PATH_INFO'], 1));
        } else {
            if(isset ($_SERVER['PATH_INFO'])) {
		$components = explode("/", substr($_SERVER['PATH_INFO'], 1));
            } else {
                if(isset($_SERVER['QUERY_STRING']) ) {
                    $components = explode("/", $_SERVER['QUERY_STRING']);
                }
            }
        }
        if(isset($components[0])) {
            if(trim($components[0]) == "") {
                    $controller = $defController;
            } else {
                $controller = $components[0];
            }
        } else {
            $controller = $defController;
        } 
        $controller = strtolower($controller)."Controller";
	if (class_exists($controller)) {
		$controller = new $controller();
		$controller->execute($components);
	} else {
		echo "Controller {$controller} no existe.";
	}

