<?php
class systemuserView extends viewBase {
    public function index() {
        templateDriver::render();
    }
    public function controlPanel() {
        templateDriver::render("systemuser.controlpanel");
    }
	public function cuenta() {
        templateDriver::render("systemuser.cuenta");
    }
}
