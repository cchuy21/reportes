<?php
class reportesView extends viewBase {
    public function index() {
        templateDriver::render();
    }
    public function extra() {
        templateDriver::render("reportes.extra");
    }
    public function mth() {
        templateDriver::render("reportes.mth");
    }
    public function clv() {
        templateDriver::render("reportes.clv");
    }
    public function faltas(){
        templateDriver::render("faltas.controlpanel");
    }
    public function personal(){
        templateDriver::render("reportes.personal");
    }
}