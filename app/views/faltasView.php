<?php
class faltasView extends viewBase {
    public function index() {
        templateDriver::render();
    }
    public function controlPanel() {
        templateDriver::render("faltas.controlpanel");
    }
	public function cuenta() {
        templateDriver::render("faltas.cuenta");
    }
}