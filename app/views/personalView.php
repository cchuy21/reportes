<?php
class personalView extends viewBase {
    public function index() {
        templateDriver::render();
    }
    public function controlPanel() {
        templateDriver::render("personal.controlpanel");
    }
	public function cuenta() {
        templateDriver::render("personal.cuenta");
    }
}