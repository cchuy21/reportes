<div class="row">
    <div class="col-xs-2 right">
        <span>Mes:</span>
    </div>
    <div class="col-xs-3">
        <select class="datepersonalm form-control">
            <option class=''></option>
            <option class='' value="1">Enero</option>
            <option class='' value="2">Febrero</option>
            <option class='' value="3">Marzo</option>
            <option class='' value="4">Abril</option>
            <option class='' value="5">Mayo</option>
            <option class='' value="6">Junio</option>
            <option class='' value="7">Julio</option>
            <option class='' value="8">Agosto</option>
            <option class='' value="9">Septiembre</option>
            <option class='' value="10">Octubre</option>
            <option class='' value="11">Noviembre</option>
            <option class='' value="12">Diciembre</option>
        </select>
        
    </div>
    <div class="col-xs-2">
        <span>Año:</span>
    </div>
    <div class="col-xs-3">
        <select class="datepersonala form-control">
            <option class=''></option>
            <?php
                $a=date("Y");
                $a++;
                for($i = 2019; $i <= $a; $i++){
                    echo "<option class='".$i."'>".$i."</option>";
                }
            ?>
        </select> 
    </div>
    <div class="col-xs-3">
        <button class='btn btn-success btn-personal'>Cargar</button>
    </div>
</div>
<div class="row contenedorpersonal">

</div>