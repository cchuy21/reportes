<?php
$u=authDriver::getUser();
?>
<div class="col-md-3 left_col" style='position:fixed'>
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="/" class="site_title"><i class="fa fa-user"></i> <span>KSMEX</span></a>
    </div>
    <div class="clearfix"></div>
    <div class="profile clearfix">
      <div class="profile_info">
        <span>Bienvenid@,</span>
        <h2><?php echo $u->username; ?></h2>
      </div>
    </div>
    <br />
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
          <li><a href="/"><i class="fa fa-edit"></i> Plantilla y Tiempo Extra </a></li>
          <li><a href="/faltas"><i class="fa fa-table"></i> Faltas</a></li>
          <li><a href="/personal"><i class="fa fa-users"></i> Personal </a></li>
          <li><a href="/rotacion"><i class="fa fa-male  "></i> Rotación</a></li>
          
        </ul>
      </div>
      <?php
      if($u->username == "admin"){
      ?>
      <div class="menu_section">
        <h3>Reportes</h3>
        <ul class="nav side-menu">
          <li><a><i class="fa fa-desktop"></i> Plantilla y Tiempo Extra <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="/reportes/MTH">MTH-C TTL</a></li>
              <li><a href="/reportes/CLV">CLV TTL</a></li>
              <li><a href="/reportes/extra">GLOBAL</a></li>

            </ul>
          </li>
          <li><a href="/reportes/faltas"><i class="fa fa-table"></i> Faltas</a></li>
          <li><a href="/reportes/personal"><i class="fa fa-users"></i> Personal </a></li>
          <li><a href="/reportes/rotacion"><i class="fa fa-male  "></i> Rotación</a></li>
        </ul>
      </div>
    <?php
    }
    ?>
    </div>
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Logout" href="/systemuser/logout">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
  </div>
</div>
<div class="top_nav">
  <div class="nav_menu">
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>
  </div>
</div>