<?php
class systemuserController extends controllerBase {
	public function emp(){
		$us=User::find("all",array("conditions"=>"isbusiness=1"));
		foreach($us as $u){
			$u->business_id=$u->id;
			$u->save();
		}
	}
    public function index() {
        if( authDriver::isLoggedin() ) {
            $this->getView()->controlPanel();
        } else {
            $this->getView()->index();
        }
    }
    public function getform($id) {
        templateDriver::setData("id", array("id" => $id));
        templateDriver::renderSection("systemuser.form");
    }
	 public function getype() {
        $su=authDriver::getSUser();
		echo json_encode($su->group);
    }
	public function getcontroluser() {
        templateDriver::renderSection("systemuser.controluser");
    }
    public function publish(){
        $data = inputDriver::getVar(__POST__);
        if($data['id'] == '' || $data['id'] == '-1' || $data['id'] == -1) {
            unset($data['id']);
            $this->register($data);
        } else {
            $this->update($data);
        }
    }
    public function validatePass($pwd = null) {
        $strength = array("Blank","Very Weak","Weak","Medium","Strong","Very Strong");
        $score = 1;
        if (strlen($pwd) < 1) {
            return 0;
        }
        if (strlen($pwd) < 4) {
            return $score;
        } 
        if (strlen($pwd) >= 8) {
            $score++;
        }

            $score++;
        
            $score++;
//realiza una comparación, si en la variable exite algún número
        if (preg_match("/[0-9]/", $pwd)) {
            $score++;
        }
        
            $score++;
        
        return $score;
    }


	//registrar a un usuario
    public function register($data=null){ 
		if($data == null) exit;
		
		$ps1=$data['password'];
		$ps2=$data['password2'];
		//convierte en mayuscula el password
        $data=$this->touppe($data); 
		
		$data['password2']=$ps2;
		$data['password']=$ps1;
        
			//si las contraseñas no coinciden manda un error
            if($data['password'] !== $data['password2']) { 
                responseDriver::dispatch('E', "Las contraseñas no coinciden", "Debes usar la misma contraseña en los dos campos");
            } else {
                unset($data['password2']);
			//devuelve un resultado en hexadecimal
				$data['password']=md5($data['password']); 
            }
                   
        $systemuser = new Systemuser($data);
		//se guarda el usuario nuevo, se agrega quien creo el usuario
		$hoy=date("Y-m-d G:i"); 
		$systemuser->log="'Creado','".authDriver::getSUser()->name." ".authDriver::getSUser()->lastname."','".$hoy."'";
		
        if(!$systemuser->save()){
            responseDriver::dispatch('E', "Creacion de usuario", "No se ha podido crear usuario del sistema intente nuevamente");	
        }else{
            responseDriver::dispatch('M', "Success", "Usuario creado exitosamente");
        }
    }   



//se modifica un usuario existente
    public function update($data){
    	$hoy=date("Y-m-d G:i"); 
        if(!isset($data['id'])) exit;
        //extrae los datos del usuario
        $systemuser = Systemuser::find_by_id($data['id']); 
		//si el password no esta definido y es null, o esta vacio 
		if(!isset($data['password'])||$data['password']==""){ 
			unset($data['password2']);
        $em=$data['email'];
		$em=$data['email'];
		$ps1=$data['password'];
		$ps2=$data['password2'];
        $data=$this->touppe($data);
		$data['email']=$em;
		$data['password2']=$ps2;
		$data['password']=$ps1;
		unset($data['password']);
			unset($data['password2']);
			//ingresar datos de quien edito un usuario
		$systemuser ->log="||'Editado','".authDriver::getSUser()->name." ".authDriver::getSUser()->lastname."','".$hoy."'";
		$systemuser->update_attributes($data);
        if(!$systemuser->errors){
            responseDriver::dispatch('E', 'Error', 'Se ha generado un errror al actualizar los datos del usuario del systema por favor intentelo de nuevo');
        }else{

            responseDriver::dispatch('M', 'Success', 'Se han actualizado correctamente los datos del usuario del sistema');
        }
            
        }else{
        	//cuando las contrseñas son incorrectas manda un mensaje al usuario
        	if($data['password'] !== $data['password2']) {
                responseDriver::dispatch('E', "Las contraseñas no coinciden", "Debes usar la misma contraseña en los dos campos");
            } else {
                unset($data['password2']); //se vacia el campo password2
				$data['password']=md5($data['password']);
            }
            
            $em=$data['email'];
			$ps1=$data['password'];
			$ps2=$data['password2'];
	        $data=$this->touppe($data);
			$data['email']=$em;
			$data['password2']=$ps2;
			$data['password']=$ps1;
			unset($data['password2']);
			$systemuser->update_attributes($data);
	        if(!$systemuser->errors){
	            responseDriver::dispatch('E', 'Error', 'Se ha generado un errror al actualizar los datos del usuario del systema por favor intentelo de nuevo');
	        }else{
	
	            responseDriver::dispatch('M', 'Success', 'Se han actualizado correctamente los datos del usuario del sistema');
	        }
        }
    } 
//eliminar a un usuario del sistema
    public function delete() {
        $data = inputDriver::getVar("systemuser");
        $systemuser = Systemuser::find_by_id($data['id']);
        if($systemuser)
            $systemuser->delete();
    }	

//al momento de iniciar sesión en el sistema
    public function login() {
        if( authDriver::isLoggedin() ) {
			header("Location: /");
			exit;
        }
        
		//ingresar al sistema correctamente
        if( authDriver::login($_POST['user']) ) {
			header("Location: /");
        } else {
			header("Location: /");
        }
    }

//salir del sistema
    public function logout() {
        authDriver::chkLoggin();
        authDriver::logout();
        redirectDriver::toController();
    }
	
	//deshabilitar a un usuario del sistema
     public function disable() {
        //Validar login
        $data = inputDriver::getVar("aid");
        $sys = Systemuser::find_by_id($data);
        if($sys){
        	
            $sys->delete();
            responseDriver::dispatch('D', array('message'=>"usuario del sistema borrado"));
        }
    }
	 public function iduser(){
	 	echo json_encode(authDriver::getSUser()->id);
	 }
	 
	 //convertir un array de minusculas a mayusculas
 	public function touppe($array){
		$ke=array_keys($array);
		
		for($i=0;$i<count($array);$i++){
			$array[$ke[$i]]=strtoupper($array[$ke[$i]]);
		}
		return $array;
		
	}
	
	public function cuenta(){
		$this->getView()->cuenta();
	}
	public function password(){
		$pass = $_POST['d'];
		$pass=md5($pass);
		//obtiene el password del usuario
		if($pass==authDriver::getSUser()->password){
			$v= "si";
		}else{
			$v= "no";
			echo authDriver::getSUser()->password;
		}
		echo json_encode($v);
	}
		
}
