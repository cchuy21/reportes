<?php
class homeController extends controllerBase {
    
   public function getunit2($id=0,$v=0,$tkn=''){
       $active=0;
        if($id!=0){
            $p=Phone::find_by_device_id($id,array("select"=>"id,version,token,driver1,driver2"));
            if(count($p)){
                $p->version=$v;
                if($tkn==""){
                }else{
                    $p->token=$tkn;
                }
                $p->save();
                if($p->driver1){
                    $cho=Operator::find_by_id($p->driver1,array("select"=>"name,lastname,id"));
                    $dri=Unity::find_by_driver($cho->id,array("select"=>"economic,id"));
                }else if($p->driver2){
                    $cho=Operator::find_by_id($p->driver2,array("select"=>"name,lastname,id"));
                    $dri=Unity::find_by_driver($cho->id,array("select"=>"economic,id"));
                }else{
                    echo json_encode(array(array("id"=>"0","eco"=>"0","cho"=>"0"))); 
                }
                if(!$dri){
                    $cho=Operator::find_by_id($p->driver2,array("select"=>"name,lastname,id"));
                    $dri=Unity::find_by_driver($cho->id,array("select"=>"economic,id"));
                }
                if(count($cho)){
                    
                    $fecha1=date("Y-m-d");
                    $cas=Assignment::find("all",array("select"=>"id,reservation_id,ticket_id","conditions"=>"operator_id='".$cho->id."' AND (created_at BETWEEN {ts'".$fecha1." 00:00:00'} AND {ts'".$fecha1." 23:59:59'})"));
                    $ct=0;
                    foreach($cas as $ca){
                        $res=Reservation::find_by_id($ca->reservation_id);
                        if($res && $res->status==3){
                            $ct++;
                        }else{
                            $res=Ticket::find_by_id($ca->ticket_id);
                            if($res && $res->status==3){
                                $ct++;
                            }
                        }
                    }  
                    
                    if(!$dri){
                        echo json_encode(array(array("id"=>$p->id,"eco"=>"0 | V: ".$ct,"cho"=>$cho->name." ".$cho->lastname,"active"=>$active)));
                    }else{
                        echo json_encode(array(array("id"=>$p->id,"eco"=>"".$dri->economic." | V: ".$ct,"cho"=>$cho->name." ".$cho->lastname,"active"=>$active)));
                    }
                }
            }else{
                $ph = new Phone();
                $ph->device_id=$id;
                $ph->token=$tkn;
                $ph->version=$v;
                $ph->save();
                echo json_encode(array(array("id"=>"0","eco"=>"0","cho"=>"0","active"=>$active))); 
            }
        }
    }

    public function device($di) {
        //Validar login
        $p=Phone::find_by_device_id($di,array("select"=>"id"));
        $dri=Operator::find_by_phone_id($p->id,array("select"=>"id"));
        $u=Unity::find_by_driver($dri->id,array("select"=>"id"));
        echo json_encode(array($p->id));
    }
    public function nborrar(){
        $re=Reservation::find("all",array("conditions"=>"reservation_date='2018-10-26'"));
        foreach($re as $r){
            $s=Service::find_by_reservation2($r->id);
            if(!$s){
                $s=Service::find_by_reservation1($r->id);
            }
            if(!$s){
                echo $s->id."<br />";
            }
        }
    }
    public function gposic(){

        $uni=Unity::find("all",array("select"=>"id,lat,lng,latlng,phone_id,dt,bat,created_at,status,driver,economic,status","conditions"=>"phone_id<>'' || phone_id<>'0'"));
        $arr=array();
        $c=0;
        $de=0;
        $dt=date("Y-m-d H:i:s");
        
        foreach($uni as $un){
            $hr=0; 
            $ca=0;
            $ser=Assignment::find("all",array("select"=>"reservation_id,ticket_id,updated_at","conditions"=>"unity_id='".$un->id."'","limit"=>"3","order"=>"id desc"));
            
            foreach($ser as $se){
                if($se->reservation_id){
                    $reser=Reservation::find($se->reservation_id);
                }else{
                    $reser=Ticket::find($se->ticket_id);
                }
                if($reser->status==2){
                    $dt2=date("Y-m-d H:i:s",strtotime($se->updated_at));
                    $date1=date_create($dt2);
                    $date2=date_create($dt);
                    $diff=date_diff($date1,$date2);
                    if($diff->format('%h')>=2){
                        $hr=1;
                    }
                    
                    $ca++;
                }
            }
            $op=Operator::find($un->driver,array("select"=>"id,name,lastname"));
            if($op){
                if($un->lat==0){
                    continue;
                }
                if($un->driver!=""){
                    $nm=$op->name." ".$op->lastname;
                }else{
                    $nm="";
                }
                $arr[$c]["id"]=$un->economic;
                $dev=Phone::find($un->phone_id); 
                $arr[$c]["nombre"]=$nm;
                $arr[$c]["numero"]=$dev->number;
                $con=1;
                $arr[$c]["lat"]=$un->lat;
                $arr[$c]["bat"]=$un->bat;
                $arr[$c]["ver"]=0;
                if($dev->version==15 || $dev->version=="15"){
                    $arr[$c]["ver"]=1;
                }
                $arr[$c]["lng"]=$un->lng;
                $arr[$c]["fecha"]=date("Y-m-d H:i:s",strtotime($un->dt));
                $ex=explode("||",$un->latlng);
                $arr[$c]["rot"]=$this->getRhumbLineBearing(floatval($ex[0]),floatval($ex[1]),floatval($un->lat),floatval($un->lng)); 
                if($ca==0){
                    $un->status=1;
                }else if($ca==1){
                    $un->status=2;
                }else{
                    $un->status=3;
                }
                if($hr>=1){
                    $un->status=4;
                }
                if($un->dt==""){
                    $un->status=0;
                }else{
                    $dar=date("Y-m-d H:i:s");
                    $d1=strtotime($un->dt)."-";
                    $tsdar=strtotime($dar);
                    $lim=$tsdar-$d1;
                   
                    if($lim<0){
                        $lim=$d1-$tsdar;
                    }
                    if($lim>600){
                        $un->status=0;
                    }
                }
                $arr[$c]["status"]=$un->status;
                $c++;
            }
        }
        echo json_encode($arr);
    }
    public function gettiempo($id){
        $res=Service::find_by_sql("SELECT * FROM `services` WHERE reservation1='".$id."' or reservation2='".$id."'");
        $ass=Assignment::find_by_sql("SELECT * FROM `assignments` WHERE reservation_id='".$id."'");
        $set=Servicetraking::find_by_sql("SELECT * FROM `servicetrakings` WHERE assignment_id='".$ass[0]->id."'");
        $trids = explode("||", $set[0]->traking_ids);
        $tt=0;
        foreach($trids as $tid){
            $tr=Traking::find_by_sql("SELECT * FROM `trakings` WHERE id='".$tid."'");
            $tt+=floatval($tr[0]->dist);
        }
        
        $datetime1 = new DateTime(date("Y-m-d G:i:s",strtotime($ass[0]->created_at)));
        $datetime2 = new DateTime(date("Y-m-d G:i:s"));
        $interval = $datetime1->diff($datetime2);
        echo json_encode(array(array("id"=>$res[0]->id,"tiempo"=>"    ".$interval->format("%H:%I:%S"),"distancia"=>"    ".round($tt,2)." km")));
        /*
        $res=Service::find_by_sql("SELECT * FROM `services` WHERE reservation1='".$id."' or reservation2='".$id."'");
        $ass=Assignment::find_by_sql("SELECT * FROM `assignments` WHERE reservation_id='".$id."'");
        $tt=0;
        $trids=Traking::find_by_sql("SELECT * FROM `trakings` WHERE unity_id='".$ass[0]->unity_id."' and operator='".$ass[0]->operator_id."' and (created_at BETWEEN {ts ''} and {ts ''})");
        foreach($trids as $tr){
            $tt+=floatval($tr->dist);
        }
        $datetime1 = new DateTime(date("Y-m-d G:i:s",strtotime($ass[0]->created_at)));
        $datetime2 = new DateTime(date("Y-m-d G:i:s"));
        $interval = $datetime1->diff($datetime2);
        echo json_encode(array(array("id"=>$res[0]->id,"tiempo"=>"    ".$interval->format("%H:%I:%S"),"distancia"=>"    ".round($tt,2)." km")));

        */
    }
    public function ver(){
        $un=Unity::find("all",array("conditions"=>"phone_id<>0"));
        echo "<table><tr><td>id_operador</td><td>ID</td><td>Chofer</td><td>phone unidad</td><td>phone operador</td></tr>";
        foreach($un as $u){
            if($u->driver==""){
                continue;
            }
            $dr=Operator::find($u->driver);
            $p=Phone::find($u->phone_id);
            echo "<tr><td>".$dr->id."</td><td>".$p->device_id."</td><td>".$dr->name." ".$dr->lastname."</td><td>".$u->phone_id."</td><td>".$dr->phone_id."</td></tr>";
        }
    }
    public function getunit($id=0){
        if($id!=0){
            $p=Phone::find_by_device_id($id,array("select"=>"id"));
            $cho=Operator::find_by_phone_id($p->id,array("select"=>"name,lastname,id"));
            $dri=Unity::find_by_driver($cho->id,array("select"=>"economic,id"));
            if(!$dri){
                echo json_encode(array(array("id"=>$p->id,"eco"=>"0","cho"=>$cho->name." ".$cho->lastname)));
            }else
            echo json_encode(array(array("id"=>$p->id,"eco"=>"".$dri->economic,"cho"=>$cho->name." ".$cho->lastname)));
        }
    }
    public function detectterminal($id){
        $u=Unity::find($id);
        if($u->ocupado==1){
            echo json_encode(array(array("id"=>$u->terminal)));
        }
    }
    public function deleterminal($id){
        $u=Unity::find($id);
        if($u->ocupado==1){
            $u->ocupado=0;
            $u->save();
        }
    }
    public function selectterm($id){
        $u=Unity::find($id);
        $u->ocupado=1;
        $u->terminal="A";
        $u->save();
    }
    public function getdis(){
        echo json_encode(array(array("dis"=>"0","min"=>"30000")));// min 6000
    }
    public function index() {
        //Validar login
        echo json_encode(array(array("id"=>293)));
    }
    public function exceso() {
        //Validar login
        echo json_encode(array(array("id"=>293)));
    }
    
    public function cerrar($md5) {
        $dat=date("Y-m-d");
        $nuevafecha = strtotime ( '-1 day' , strtotime ( $dat ) ) ;
        $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
        $ex=explode("-",$md5);
        $p=Operator::find_by_phone_id($ex[0],array("select"=>"id,list"));
        $un=Unity::find_by_driver($p->id);
        $re=Assignment::find("all",array("conditions"=>"unity_id='".$un->id."' and created_at > {ts '".$nuevafecha." 00:00:00'}"));
        foreach($re as $r){
            if($ex[1]==md5("kaino_".$r->reservation_id)){
                $rf= $r->reservation_id;
                $e="reser";
                break;
            }else if($ex[1]==md5("kaino_".$r->ticket_id)){
                $rf= $r->ticket_id;
                $e="tic";
                break;
            }
        }
        if($e=="reser"){
            $reser=Reservation::find_by_id($rf);
			
			/*if($reser->type=="APTO-MTY"){
				$reser->reservation_date=date("Y-m-d");
				$reser->reservation_time=date("G:i");
            }*/
            
            $reser->status=3;
			$reser->save();
            $ass=Assignment::find_by_reservation_id($rf);
           
			$un=Unity::find_by_id($ass->unity_id);
			$un->status=2;
			$un->save();
			
			$ser=Service::find_by_reservation1($reser->id);
			$ser->chargedby=substr(authDriver::getSUser()->name, 0,1).substr(authDriver::getSUser()->lastname, 0,1);
			$fe=date("Y-m-d G:i");
            $ser->log=$ser->log."||'Cobrado','".authDriver::getSUser()->name."','".$fe."'";
            $ser->save();
        }else{
            $c=0;
            $reser=Ticket::find_by_id($rf);
            $reser->status=3;
            if($reser->save()){
                $c++;
            }
            $ass=Assignment::find_by_ticket_id($rf);
            $un=Unity::find_by_id($ass->unity_id);
            $un->status=2;
            if($un->save()){
                $c++;
            }
            if($c==2){
                $p->list=($ope->list+1);
                $p->save();
                responseDriver::dispatch('D', array('message'=>"terminada"));
            }
        }
    }
    public function ocupacion($fecha=false,$hora=false,$tipo=false,$reserva=false){
        
        
        
        if(!$fecha)
            $fecha=$_POST["fecha"];
        if(!$hora)
            $hora=$_POST["hora"];
        if(!$tipo)
            $tipo=$_POST["tipo"];
        if(!$reserva)
            $reserva=$_POST["reserva"];
            
        
        
        $fecha1=date("Y-m-d",strtotime($fecha));
        
        $hora1=date("H:i:s",strtotime($hora));
        
        $hrs8 = strtotime ( '+8 hour' , strtotime ( date("Y-m-d H:i:s") ) ) ;

        if($hrs8>strtotime($fecha1." ".$hora1)){
            echo json_encode(array("disponibles"=>0,"idrecom"=>"8 hrs"));
            exit;
        }
        if($tipo=="APTO-MTY"){
            echo json_encode(array("disponibles"=>1));
            exit;
        }
        //echo $fecha1." ".$hora1;
        $sc=Schedule::find("all",array("select"=>"id,vn,vl,ln,ll,hour,hour1,hour2","conditions"=>"hour1<={ts '".$hora1."'} and hour2>={ts '".$hora1."'}"));
        if(!count($sc)){
            echo json_encode(array("disponibles"=>1));exit;
        }
        //print_r($sc);
        $s=$sc[0];
        $res=Reservation::find("all",array("select"=>"id", "conditions"=>"status <> 0 and reservation_date='".$fecha1."' AND reservation_time BETWEEN {ts '".$s->hour1."'} AND {ts '".$s->hour2."'} AND type='MTY-APTO'"));
        $cant=0;
        
        foreach($res as $r){
            $ser=Service::find_by_reservation1($r->id,array("select"=>"id"));
            if(!$ser)
                $ser=Service::find_by_reservation2($r->id,array("select"=>"id"));
            //$u=$ser->user;
            if($ser){
                if($ser->id!=$reserva)
                $cant++;
                else{
                   
                }
            }
        }
        $tenta=0;
        $rest=Restriction::find("all",array("conditions"=>"fecha ={ts '".$fecha1."'} and schedule_id='".$s->id."'"));
        $rst=$rest[0];
        
        $w=date("w",strtotime($fecha1));
        
        if($w>1 && $w<6){
            $tenta=$s->vn;            
        }else{
            $tenta=$s->ln;
        }
        if($rst->cambio>0){
            $tenta=$rst->cambio;
        }
        
        $total=$cant+intval($rst->alt)+intval($rst->vol)+intval($rst->amx)+intval($rst->int)+intval($rst->extra);
        $dispo=intval($tenta)-intval($total);
        if($dispo>0){
            echo json_encode(array("disponibles"=>$dispo));
        }else{
            echo json_encode(array("disponibles"=>0,"idrecom"=>$rst->id));
        }
        //print_r($rst);
    }
    public function recomendaciones($id){
        if($id=="8 hrs"){
            echo "Horario restringido";
            exit;
        }
        $rest=Restriction::find($id);
        $sc=$rest->schedule_id;
        echo "Lo sentimos no hay disponibles para este horario<br />";
        $fecha=date("Y-m-d",strtotime($rest->fecha));
        for($i=($sc-1);$i>0;$i--){
            $re=Restriction::find("all",array("conditions"=>"fecha ={ts '".$fecha."'} and schedule_id='".$i."'"));
            if(count($re)){
                $s=Schedule::find($i,array("select"=>"id,vn,vl,ln,ll,hour,hour1,hour2"));
                $res=Reservation::find("all",array("select"=>"id", "conditions"=>"status <> 0 and reservation_date='".$fecha."' AND reservation_time BETWEEN {ts '".$s->hour1."'} AND {ts '".$s->hour2."'} AND type='MTY-APTO'"));
                $cant=0;
                foreach($res as $r){
                    $ser=Service::find_by_reservation1($r->id,array("select"=>"id"));
                    if(!$ser)
                        $ser=Service::find_by_reservation2($r->id,array("select"=>"id"));
                    //$u=$ser->user;
                    if($ser){
                        $cant++;
                    }
                }
                $w=date("w",strtotime($fecha));
                if($w>1 && $w<6){
                    $tenta=$s->vn;            
                }else{
                    $tenta=$s->ln;
                }
                if($rst->cambio>0){
                    $tenta=$rst->cambio;
                }
                $total=$cant+intval($rst->alt)+intval($rst->vol)+intval($rst->amx)+intval($rst->int)+intval($rst->extra);
                $dispo=intval($tenta)-intval($total);
                if($dispo){
                    echo "Existen ".$dispo." en horario de ".$s->hour."<br />";
                    break;
                }else{
                    
                }
            }
        }
        for($i=($sc+1);$i<=9;$i++){
            $re=Restriction::find("all",array("conditions"=>"fecha ={ts '".$fecha."'} and schedule_id='".$i."'"));
            if(count($re)){
                $s=Schedule::find($i,array("select"=>"id,vn,vl,ln,ll,hour,hour1,hour2"));
                $res=Reservation::find("all",array("select"=>"id", "conditions"=>"status <> 0 and reservation_date='".$fecha."' AND reservation_time BETWEEN {ts '".$s->hour1."'} AND {ts '".$s->hour2."'} AND type='MTY-APTO'"));
                $cant=0;
                foreach($res as $r){
                    $ser=Service::find_by_reservation1($r->id,array("select"=>"id"));
                    if(!$ser)
                        $ser=Service::find_by_reservation2($r->id,array("select"=>"id"));
                    //$u=$ser->user;
                    if($ser){
                        $cant++;
                    }
                }
                $w=date("w",strtotime($fecha));
                if($w>1 && $w<6){
                    $tenta=$s->vn;            
                }else{
                    $tenta=$s->ln;
                }
                if($rst->cambio>0){
                    $tenta=$rst->cambio;
                }
                $total=$cant+intval($rst->alt)+intval($rst->vol)+intval($rst->amx)+intval($rst->int)+intval($rst->extra);
                $dispo=intval($tenta)-intval($total);
                if($dispo){
                    echo "Existen ".$dispo." en horario de ".$s->hour;
                    break;
                }else{
                    
                }
            }
        }
    }
    public function ordern($op){
		$p=Operator::find("all",array("select"=>"list,id","limit"=>"1","order"=>"list desc"));
		
		$ope=Operator::find($op);
		$ope->list=($p[0]->list+1);
		$ope->save();

	}
    public function sendnot($phe,$idres){

        $server_key='AIzaSyDmVM6ZPCPIJiPpDbfjK4w9_Air_xUd3IU';
        $headers = array(
            'Authorization:key='.$server_key,
            'Content-Type:application/json'
        );
        $ph=Phone::find("all",array("conditions"=>"id='".$phe."'"));
        if(count($ph)){
            $p=Phone::find($phe);
            //key token
            $key=$p->token;
            $fields = array('to'=>$key,'data'=>array('prueba'=>$idres));
            $payload = json_encode($fields);
            $curl_session = curl_init();
            $path_to_fcm='https://fcm.googleapis.com/fcm/send';
            curl_setopt($curl_session, CURLOPT_URL,$path_to_fcm);
            curl_setopt($curl_session, CURLOPT_POST,true);
            curl_setopt($curl_session, CURLOPT_HTTPHEADER,$headers);
            curl_setopt($curl_session, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl_session, CURLOPT_SSL_VERIFYPEER,false);
            curl_setopt($curl_session, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($curl_session, CURLOPT_POSTFIELDS,$payload);
            $result = curl_exec($curl_session);
            curl_close($curl_session);
        }
    }
    public function cerrar2($md5) {
        header('Content-type: application/json');
        $dat=date("Y-m-d");
        $nuevafecha = strtotime ( '-1 day' , strtotime ( $dat ) ) ;
        $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
        $ex=explode("-",$md5);
        $ph=Phone::find($ex[0],array("select"=>"driver1,driver2,id"));
        
        $p=Operator::find($ph->driver1,array("select"=>"id"));
        if(!$p){
            
            $p=Operator::find($ph->driver2,array("select"=>"id"));
        }else{
            $un=Unity::find_by_driver($p->id);
            if(!$un){
                $p=Operator::find($ph->driver2,array("select"=>"id"));
            }
        }
        $un=Unity::find_by_driver($p->id);

        $re=Assignment::find("all",array("conditions"=>"unity_id='".$un->id."' and created_at > {ts '".$nuevafecha." 00:00:00'}"));
        foreach($re as $r){
            if($ex[1]==md5("kaino_".$r->reservation_id)){
                $rf= $r->reservation_id;
                $e="reser";
                break;
            }else if($ex[1]==md5("kaino_".$r->ticket_id)){
                $rf= $r->ticket_id;
                $e="tic";
                break;
            }
        }
        
        if($e=="reser"){
            $reser=Reservation::find_by_id($rf);
			$reser->status=3;
			/*if($reser->type=="APTO-MTY"){
				$reser->reservation_date=date("Y-m-d");
				$reser->reservation_time=date("G:i");
			}*/
            $ass=Assignment::find_by_reservation_id($rf);
            $rw=Servicetraking::find("all",array("conditions"=>"assignment_id='".$ass->id."'"));
            if(count($rw)){
                foreach($rw as $rwq){
                    $rw1=$rwq;
                }
                $rw1->status='2';
                $rw1->closed_time=date("Y-m-d G:i:s");
                $rw1->save();
            }
			$un=Unity::find_by_id($ass->unity_id);
			$un->status=2;
			//$un->save();
            $ser=Service::find_by_reservation1($reser->id);
            if(!$ser){
                $ser=Service::find_by_reservation2($reser->id);
            }
			$fe=date("Y-m-d G:i");
            //$ser->save();
            
            if($reser->is_valid() && $un->is_valid() && $ser->is_valid()){
                $reser->save();
                $un->save();
                $this->ordern($p->id);
                
                echo json_encode(array(array("status"=>"1")));
                $this->sendnot($ex[0],$rf);
                exit;
            }else{
                echo json_encode(array(array("status"=>"0")));
                exit;
            }
        }else if($e=="tic"){
            $c=0;
            $reser=Ticket::find_by_id($rf);
            $reser->status=3;
            
            $ass=Assignment::find_by_ticket_id($rf);
            $un=Unity::find_by_id($ass->unity_id);
            $un->status=2;
            
            if($reser->is_valid() && $un->is_valid() && $p->is_valid()){
                $reser->save();
                $un->save();
                $this->ordern($p->id);
                
                echo json_encode(array(array("status"=>"1")));
                exit;
            }else{
                echo json_encode(array(array("status"=>"0")));
                exit;
            }
        }else{
            echo json_encode(0);
            exit;
        }
    }
    public function operadores($id="9025"){
        $op=Operator::find($id);
        $as=Assignment::find("all",array("conditions"=>"created_at > {ts '2018-09-28 00:00:00'} and operator_id='".$id."'"));
        foreach($as as $a){
            if($a->reservation_id){
                $s=Service::find_by_reservation1($a->reservation_id);    
            }
            if(!$s){
                $s=Service::find_by_reservation2($a->reservation_id);
            }
            if($s->user->name=="PRUEBAS DE SISTEMA"){
                $r=Reservation::find($a->reservation_id);
                $r->status=0;
                $r->save();
                $a->delete();
                echo $r->id."***<br />";
            }
            
        }
    }
    public function detectclose($md5){
        $dat=date("Y-m-d");
        $nuevafecha = strtotime ( '-1 day' , strtotime ( $dat ) ) ;
        $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
        $ex=explode("-",$md5);
        $re=Assignment::find("all",array("conditions"=>"unity_id='".$ex[0]."' and created_at > {ts '".$nuevafecha." 00:00:00'}"));
        $rf;
        $e;
        foreach($re as $r){
            if($ex[1]==md5("kaino_".$r->reservation_id)){
                $rf= $r->reservation_id;
                $e="reser";
                break;
            }else if($ex[1]==md5("kaino_".$r->ticket_id)){
                $rf= $r->ticket_id;
                $e="tic";
                break;
            }
        }
        if($e=="reser"){
            $reser=Reservation::find_by_id($rf);
        }else{
            $reser=Ticket::find_by_id($rf);
        }
        if($reser->status==2 || $reser->status=="2"){
            echo json_encode(array(array("status"=>0)));
        }else{
            echo json_encode(array(array("status"=>1)));
        }
    }
    public function getservices($id) {
        //Validar login 
        $p=Phone::find($id,array("select"=>"driver1,driver2,id"));
        
        $op=Operator::find($p->driver1,array("select"=>"id"));
        $un=Unity::find_by_driver($op->id);

        if(!$op || !$un){
            $op=Operator::find($p->driver2,array("select"=>"id"));
            $un=Unity::find_by_driver($op->id);

        }
        $arr=array();
        $c=0;

            $as=Assignment::find("all",array("select"=>"id,reservation_id,ticket_id","conditions"=>"unity_id='".$un->id."'","limit"=>5,"order"=>"id desc"));
            foreach($as as $a){
                $st=Servicetraking::find("all",array("select"=>"id","conditions"=>"status='1' and assignment_id='".$a->id."'"));
                if(count($st)){
                    $ac=1;
                }  else{
                    $ac=0;
                }
                if(isset($a->ticket_id) && $a->ticket_id!=""){
                    $t=Ticket::find($a->ticket_id,array("select"=>"id,folio,zone,terminal,created_at,time,status"));
                    if($t->status==2){
                        $arr[$c]["id"]=$t->id;
                        $arr[$c]["ids"]=$t->folio;
                        $arr[$c]["tipo"]=2;
                        $arr[$c]["active"]=$ac;
                        $arr[$c]["latlon"]=",";
                        $arr[$c]["pasajero"]="";
                        $arr[$c]["observaciones"]="";
                        $arr[$c]["direccion"]=$t->zone;
                        $arr[$c]["direccion1"]=$t->zone;
                        $arr[$c]["direccion2"]="";
                        $arr[$c]["telefono"]="";
                        $arr[$c]["pago"]="";
                        if($t->terminal=="NAC"){
                            $t->terminal="A";
                        }else if($t->terminal=="TERM_B"){
                            $t->terminal="B";
                        }else if($t->terminal=="TERM_C"){
                            $t->terminal="C";
                        }
                        $arr[$c]["terminal"]=$t->terminal;
                        $arr[$c]["fecha"]=$this->transfecha(date("Y-m-d H:i",strtotime($t->created_at)));
                        $arr[$c]["type"]="";
                        $arr[$c]["time"]=date("H:i",strtotime($t->time));
                        $c++;
                    }
                }else{
                    $st=Servicetraking::find("all",array("select"=>"id","conditions"=>"status='1' and assignment_id='".$a->id."'"));  
                    if(count($st)){
                        $ac=1;
                    }else{
                        $ac=0;
                    }    
                    $r=Reservation::find($a->reservation_id,array("select"=>"id,status,terminal,reservation_date,reservation_time,type,addresses"));
                    if($r->status==2 || $r->status==5){ 
                        $arr[$c]["id"]=$r->id;
						$s=Service::find_by_reservation1($r->id,array("select"=>"id,user_id,payment,annotations"));
                        if(!$s){
                            $s=Service::find_by_reservation2($r->id,array("select"=>"id,user_id,payment,annotations"));
                        }
                        $ad=Address::find(substr($r->addresses,1),array("select"=>"suburb_id,street"));
                        $sub=Suburb::find($ad->suburb_id,array("select"=>"suburb"));
                        $u=$s->user;

                        $arr[$c]["ids"]=$s->id;
                        $arr[$c]["tipo"]=1;
                        $arr[$c]["active"]=$ac;
                        $arr[$c]["latlon"]=$this->getroute($ad->street." ".$sub->suburb);
                        $arr[$c]["pasajero"]=$u->name;
                        $arr[$c]["observaciones"]=$s->annotations;
                        $arr[$c]["direccion"]=$ad->street." ".$sub->suburb;
                        $arr[$c]["direccion1"]=$ad->street;
                        $arr[$c]["direccion2"]=$sub->suburb;
                        $arr[$c]["telefono"]=$u->telephones;
                        if($s->payment=="Tarjeta de Credito/Debito" || $s->payment=="Efectivo MN"){
                            $s->payment="Paga en Caseta";
                        }else{
                            if($r->terminal=="null" || $r->terminal==null){
                                $s->payment="Sin Terminal";
                            }else
                                $s->payment="Directo a ".$r->terminal;
                        }
                        $arr[$c]["pago"]=$s->payment;
                        $arr[$c]["terminal"]=$r->terminal;
                        $arr[$c]["fecha"]=$this->transfecha(date("Y-m-d H:i",strtotime(date("Y-m-d",strtotime($r->reservation_date))." ".date("H:i",strtotime($r->reservation_time)))));
                        if($r->type=="MTY-APTO"){
                            $tpd="Salida";
                        }else{
                            $tpd="Llegada";
                        }
                        $arr[$c]["type"]=$tpd;
                        $arr[$c]["time"]=date("H:i",strtotime($r->reservation_time));
                        $c++;
                    }
                }
            }
            
        echo json_encode($arr);
    }
    public function getservices4($id) {
        //Validar login
        
        //$un=Unity::find_by_phone_id($id);
        $p=Phone::find($id,array("select"=>"driver1,driver2,id"));
        
        $op=Operator::find($p->driver1,array("select"=>"id"));
        if(!$op)
            $op=Operator::find($p->driver2,array("select"=>"id"));
        $uni=Unity::find_by_sql("select id from `unities` where driver='".$op->id."'");
        $un=$uni[0];
        $arr=array();
        $c=0;
        $as=Assignment::find_by_sql("select id,reservation_id,ticket_id from `assignments` where unity_id='".$un->id."' ORDER BY id DESC LIMIT 3");
        foreach($as as $a){
                    $ac=1;
                if(isset($a->ticket_id) && $a->ticket_id!=""){
                    $ti=Ticket::find_by_sql("select id,folio,zone,terminal,created_at,time,status from `tickets` where id='".$a->ticket_id."'");
                    $t=$ti[0];
                    if($t->status==2){
                        $arr[$c]["id"]=$t->id;
                        $arr[$c]["ids"]=$t->folio;
                        $arr[$c]["tipo"]=2;
                        $arr[$c]["active"]=$ac;
                        $arr[$c]["latlon"]=",";
                        $arr[$c]["pasajero"]="";
                        $arr[$c]["observaciones"]="";
                        $arr[$c]["direccion"]=$t->zone;
                        $arr[$c]["direccion1"]=$t->zone;
                        $arr[$c]["direccion2"]="";
                        $arr[$c]["telefono"]="";
                        $arr[$c]["pago"]="";
                        if($t->terminal=="NAC"){
                            $t->terminal="A";
                        }else if($t->terminal=="TERM_B"){
                            $t->terminal="B";
                        }else if($t->terminal=="TERM_C"){
                            $t->terminal="C";
                        }
                        $arr[$c]["terminal"]=$t->terminal;
                        $arr[$c]["fecha"]=$this->transfecha(date("Y-m-d H:i",strtotime($t->created_at)));
                        $arr[$c]["type"]="";
                        $arr[$c]["time"]=date("H:i",strtotime($t->time));
                        $c++;
                    }
                }else{
                    $ac=1;
                    $ri=Reservation::find_by_sql("select id,status,terminal,reservation_date,reservation_time,type,addresses from `reservations` where id='".$a->reservation_id."'");
                    $r=$ri[0];
                    if($r->status==2 || $r->status==5){ 
                        $arr[$c]["id"]=$r->id;
                        $si=Service::find_by_sql("select id,user_id,payment,annotations from `services` where reservation1='".$r->id."'");
                        $s=$si[0];
                        if(!$s){
                            $si=Service::find_by_sql("select id,user_id,payment,annotations from `services` where reservation2='".$r->id."'");
                            $s=$si[0];
                        }
                        
                        $ad=Address::find(substr($r->addresses,1),array("select"=>"suburb_id,street"));

                        $sub=Suburb::find($ad->suburb_id,array("select"=>"suburb"));
                        $u=$s->user;

                        $arr[$c]["ids"]=$s->id;
                        $arr[$c]["tipo"]=1;
                        $arr[$c]["active"]=$ac;
                        $arr[$c]["latlon"]=$this->getroute($ad->street." ".$sub->suburb);
                        $arr[$c]["pasajero"]=$u->name;
                        $arr[$c]["observaciones"]=$s->annotations;
                        $arr[$c]["direccion"]=$ad->street." ".$sub->suburb;
                        $arr[$c]["direccion1"]=$ad->street;
                        $arr[$c]["direccion2"]=$sub->suburb;
                        $arr[$c]["telefono"]=$u->telephones;
                        if($s->payment=="Tarjeta de Credito/Debito" || $s->payment=="Efectivo MN"){
                            $s->payment="Paga en Caseta";
                        }else{
                            if($r->terminal=="null" || $r->terminal==null){
                                $s->payment="Sin Terminal";
                            }else
                                $s->payment="Directo a ".$r->terminal;
                        }
                        $arr[$c]["pago"]=$s->payment;
                        $arr[$c]["terminal"]=$r->terminal;
                        $arr[$c]["fecha"]=$this->transfecha(date("Y-m-d H:i",strtotime(date("Y-m-d",strtotime($r->reservation_date))." ".date("H:i",strtotime($r->reservation_time)))));
                        if($r->type=="MTY-APTO"){
                            $tpd="Salida";
                        }else{
                            $tpd="Llegada";
                        }
                        $arr[$c]["type"]=$tpd;
                        $arr[$c]["time"]=date("H:i",strtotime($r->reservation_time));
                        $c++;
                    }
                }
            }
        if($c>0){
            echo json_encode($arr);
        }else{
            $c=0;
            $arr[$c]["id"]=0;
            $arr[$c]["ids"]=0;
            $arr[$c]["tipo"]=0;
            $arr[$c]["active"]=0;
            $arr[$c]["latlon"]="";
            $arr[$c]["pasajero"]="";
            $arr[$c]["observaciones"]="";
            $arr[$c]["direccion"]="0";
            $arr[$c]["direccion1"]="0";
            $arr[$c]["direccion2"]="0";
            $arr[$c]["telefono"]="0";
            $arr[$c]["pago"]="0";
            $arr[$c]["terminal"]="0";
            $arr[$c]["fecha"]="";
            $arr[$c]["type"]="";
            $arr[$c]["time"]="";
            echo json_encode($arr);
        }
    }
    public function getservices2($id) {
        //Validar login
        $p=Phone::find($id,array("select"=>"driver1,driver2,id"));
        
        $op=Operator::find($p->driver1,array("select"=>"id,name,lastname"));
        $un=Unity::find_by_driver($op->id);

        if(!$op || !$un){
            $op=Operator::find($p->driver2,array("select"=>"id,name,lastname"));
            $un=Unity::find_by_driver($op->id);

        }
        $arr=array();
        $c=0;

            $as=Assignment::find("all",array("select"=>"id,reservation_id,ticket_id","conditions"=>"unity_id='".$un->id."'","limit"=>3,"order"=>"id desc"));
            foreach($as as $a){
                    $ac=1;
                if(isset($a->ticket_id) && $a->ticket_id!=""){
                    $t=Ticket::find($a->ticket_id,array("select"=>"id,folio,zone,terminal,created_at,time,status"));
                    if($t->status==2){
                        $arr[$c]["id"]=$t->id;
                        $arr[$c]["ids"]=$t->folio;
                        $arr[$c]["tipo"]=2;
                        $arr[$c]["active"]=$ac;
                        $arr[$c]["latlon"]=",";
                        $arr[$c]["pasajero"]="";
                        $arr[$c]["observaciones"]="";
                        $arr[$c]["direccion"]=$t->zone;
                        $arr[$c]["direccion1"]=$t->zone;
                        $arr[$c]["direccion2"]="";
                        $arr[$c]["telefono"]="";
                        $arr[$c]["pago"]="";
                        if($t->terminal=="NAC"){
                            $t->terminal="A";
                        }else if($t->terminal=="TERM_B"){
                            $t->terminal="B";
                        }else if($t->terminal=="TERM_C"){
                            $t->terminal="C";
                        }
                        $arr[$c]["terminal"]=$t->terminal;
                        $arr[$c]["fecha"]=$this->transfecha(date("Y-m-d H:i",strtotime($t->created_at)));
                        $arr[$c]["type"]="";
                        $arr[$c]["time"]=date("H:i",strtotime($t->time));
                        $c++;
                    }
                }else{
                        $ac=1;
                    $r=Reservation::find($a->reservation_id,array("select"=>"id,status,terminal,reservation_date,reservation_time,type,addresses"));
                    if($r->status==2 || $r->status==5){ 
                        $arr[$c]["id"]=$r->id;
						$s=Service::find_by_reservation1($r->id,array("select"=>"id,user_id,payment,annotations"));
                        if(!$s){
                            $s=Service::find_by_reservation2($r->id,array("select"=>"id,user_id,payment,annotations"));
                        }
                        $ad=Address::find(substr($r->addresses,1),array("select"=>"suburb_id,street"));
                        $sub=Suburb::find($ad->suburb_id,array("select"=>"suburb"));
                        $u=$s->user;

                        $arr[$c]["ids"]=$s->id;
                        $arr[$c]["tipo"]=1;
                        $arr[$c]["active"]=$ac;
                        $arr[$c]["latlon"]=$this->getroute($ad->street." ".$sub->suburb);
                        $arr[$c]["pasajero"]=$u->name;
                        $arr[$c]["observaciones"]=$s->annotations;
                        $arr[$c]["direccion"]=$ad->street." ".$sub->suburb;
                        $arr[$c]["direccion1"]=$ad->street;
                        $arr[$c]["direccion2"]=$sub->suburb;
                        $arr[$c]["telefono"]=$u->telephones;
                        if($s->payment=="Tarjeta de Credito/Debito" || $s->payment=="Efectivo MN"){
                            $s->payment="Paga en Caseta";
                        }else{
                            if($r->terminal=="null" || $r->terminal==null){
                                $s->payment="Sin Terminal";
                            }else
                                $s->payment="Directo a ".$r->terminal;
                        }
                        $arr[$c]["pago"]=$s->payment;
                        $arr[$c]["terminal"]=$r->terminal;
                        $arr[$c]["fecha"]=$this->transfecha(date("Y-m-d H:i",strtotime(date("Y-m-d",strtotime($r->reservation_date))." ".date("H:i",strtotime($r->reservation_time)))));
                        if($r->type=="MTY-APTO"){
                            $tpd="Salida";
                        }else{
                            $tpd="Llegada";
                        }
                        $arr[$c]["type"]=$tpd;
                        $arr[$c]["time"]=date("H:i",strtotime($r->reservation_time));
                        $c++;
                    }
                }
            }
        if($c>0){
            echo json_encode($arr);
        }else{
            $c=0;
            $arr[$c]["id"]=0;
            $arr[$c]["ids"]=0;
            $arr[$c]["tipo"]=0;
            $arr[$c]["active"]=0;
            $arr[$c]["latlon"]="";
            $arr[$c]["pasajero"]="";
            $arr[$c]["observaciones"]="";
            $arr[$c]["direccion"]="0";
            $arr[$c]["direccion1"]="0";
            $arr[$c]["direccion2"]="0";
            $arr[$c]["telefono"]="0";
            $arr[$c]["pago"]="0";
            $arr[$c]["terminal"]="0";
            $arr[$c]["fecha"]="";
            $arr[$c]["type"]="";
            $arr[$c]["time"]="";
            echo json_encode($arr);
        }
    }
    public function getallservices($id) {
        //Validar login
        $p=Phone::find($id,array("select"=>"driver1,driver2,id"));
        
        $op=Operator::find($p->driver1,array("select"=>"id"));
        if(!$op){
            
            $op=Operator::find($p->driver2,array("select"=>"id"));
        }else{
            $un=Unity::find_by_driver($op->id);
            if(!$un){
                $op=Operator::find($p->driver2,array("select"=>"id"));
            }
        }
        $un=Unity::find_by_driver($op->id);

        $arr=array();
        $c=0;
        $as=Assignment::find("all",array("select"=>"id,reservation_id,ticket_id,unity_id,updated_at","conditions"=>"operator_id='".$op->id."' and (created_at BETWEEN {ts '".date("Y-m-d")." 00:00:00'} and {ts '".date("Y-m-d")." 23:59:59'})","order"=>"created_at asc"));
        foreach($as as $a) {
            $sert=Servicetraking::find_by_assignment_id($a->id);
            $r=Reservation::find_by_id($a->reservation_id);
            if($r){
                $s=service::find("all",array("conditions"=>"reservation1='".$a->reservation_id."' or reservation2='".$a->reservation_id."'"));
                foreach($s as $se){
                    $ser=$se->id;
                }
                if($r->type=="MTY-APTO"){
                    $tipo="M-A";
                }else{
                    $tipo="A-M";
                }
                $un=Unity::find($a->unity_id);
                $arr[$c]["unidad"]=$un->economic;
                $fecha=date("Y-m-d",strtotime($r->reservation_date));
                $hora=date("G:i",strtotime($r->reservation_time));
            }else{
                $s=Ticket::find($a->ticket_id);
                if($s){
                    $un=Unity::find($a->unity_id);
                    $arr[$c]["unidad"]=$un->economic;
                    $ser="B: ".$s->folio;
                    $tipo="";
                    $fecha=date("Y-m-d",strtotime($s->created_at));
                    $hora=date("G:i",strtotime($s->created_at));
                }
            }
            $dia=date("d",strtotime($fecha));
            $dia=intval($dia);
            $m=date("M",strtotime($fecha));
            $arr[$c]["num"]=($c+1);
            $arr[$c]["servicio"]=$ser;
            $arr[$c]["tipo"]=$tipo;
            $date1 = $sert->start_time;
            $date2 = $a->updated_at;
            $datetime1 = new DateTime(date("Y-m-d H:i:s",strtotime($date1)));
            $datetime2 = new DateTime(date("Y-m-d H:i:s",strtotime($date2)));
            $interval = $datetime1->diff($datetime2);
            $hi=$interval->format('%H');
            $mi=$interval->format('%I');
            $si=$interval->format('%s');
            if($date1==""){
                $arr[$c]["tiempo"]="00:00";
            }else{
                $arr[$c]["tiempo"]=$hi.":".$mi;
                if($arr[$c]["tiempo"]=="00:0"){
                    $arr[$c]["tiempo"]="00:00";
                }
            }
            if($dia<10){
                $arr[$c]["fecha"]="0".$dia." ".$m." ".$hora;
            }else{
                $arr[$c]["fecha"]=$dia." ".$m." ".$hora;
            }
            $c++;
        }
        if($c>0){
            echo json_encode($arr);
        }else{
            $c=0;
            $arr[$c]["num"]='';
            $arr[$c]["unidad"]='';
            $arr[$c]["servicio"]='';
            $arr[$c]["tipo"]='';
            $arr[$c]["fecha"]='';
            $arr[$c]["tiempo"]='';
            echo json_encode($arr);
        }
    }
    public function getservices1($id=240) {
        //Validar login
        $id=16;
        $un=Unity::find($id);
        $arr=array();
        $c=0;
            $as=Assignment::find("all",array("limit"=>50,"order"=>"id desc"));
            foreach($as as $a){
                if(isset($a->ticket_id) && $a->ticket_id!=""){
                    continue;
                    $t=Ticket::find($a->ticket_id);
                    if($t->status==2){
                        $arr[$c]["id"]=$t->id;
                        $arr[$c]["ids"]=$t->id;
                        $arr[$c]["tipo"]=2;
                        $arr[$c]["latlon"]="ticket";
                        $c++;
                    }
                }else{
                    $r=Reservation::find($a->reservation_id);
                    $arr[$c]["id"]=$r->id;
                    $s=Service::find_by_reservation1($r->id);
                    if(!$s){
                        $s=Service::find_by_reservation2($r->id);
                    }
                    $ad=Address::find(substr($r->addresses,1));
                    $sub=Suburb::find($ad->suburb_id);
                    $arr[$c]["ids"]=$s->id;
                    $arr[$c]["tipo"]=1;
                    $arr[$c]["latlon"]=$this->getroute($ad->street." ".$sub->suburb);
                    $c++;
                    if($c==10){
                        break;
                    }
                }
            }
        echo json_encode($arr);
    }
    public function savepos2($di,$lat,$lon,$accur){
        $hor=(int) date("i");
        if($hor%3==0){
            $p=Phone::find_by_device_id($di); 
            $dri=Operator::find_by_phone_id($p->id);
            $un=Unity::find("all",array("conditions"=>"driver='".$dri->id."'"));
            if(count($un)){
                $dri->lat=$lat;
                $dri->lng=$lon;
                if($dri->is_valid()){
                    $dri->save();
                    echo json_encode(array(array("id"=>1)));
                }else{
                    echo json_encode(array(array("id"=>0)));
                }
            }else{
                $dri->lat=$lat;
                $dri->lng=$lon;
                if($dri->is_valid()){
                    $dri->save();
                    echo json_encode(array(array("id"=>1)));
                }else{
                    echo json_encode(array(array("id"=>0)));
                }
            }
        }
    }
    public function savepos3($di,$lat,$lon,$accur){
        $hor=(int) date("i");
        if($hor==3 || $hor==6 || $hor==9){
            $p=Phone::find_by_device_id($di);
            $dri=Operator::find_by_phone_id($p->id);
            $un=Unity::find("all",array("conditions"=>"driver='".$dri->id."'"));
            if(count($un)){
                $un=Unity::find_by_driver($dri->id);
                /*$o=Traking::find("all",array("conditions"=>"unity_id='".$un->id."' and operator='".$un->driver."' and device_id='".$p->device_id."' and lat='".$lat."' and lng='".$lon."' and created_at={ts '".date("Y-m-d H:i:s")."'}"));
                if(count($o)){
                    exit;
                }
                $tr=new Traking();
                //$lats=$lat." ";
                //$lons=$lon." ";
                //if(strlen($lats) < 14 || strlen($lons) < 14){
                //    exit;
                //}
                $tr->unity_id=$un->id;
                $tr->operator=$un->driver;
                $tr->device_id=$p->device_id;
                $tr->lat=$lat;
                $tr->lng=$lon;*/
                $dri->lat=$lat;
                $dri->lng=$lon;
                echo "entro";exit;
                if($dri->is_valid()){
                    $dri->save();
                    /*if($tr->is_valid()){
                        $st=Servicetraking::find("all",array("conditions"=>"status='1' and device_id='".$di."'","order"=>"id asc"));
                        if(count($st)){
                            $tr->save();
                            //velocidad
                            $atr=Traking::find("all",array("conditions"=>"id <> '".$tr->id."' and device_id='".$di."'","order"=>"id desc","limit"=>"1"));
                            if(count($atr)){
                                foreach($atr as $ultrack){
                                    $trant=$ultrack;
                                }
                                $datetime1 = new DateTime(date("Y-m-d H:i:s",strtotime($trant->updated_at)));
                                $datetime2 = new DateTime(date("Y-m-d H:i:s",strtotime($tr->updated_at)));
                                $interval = $datetime1->diff($datetime2);
                                $hi=$interval->format('%H');
                                $mi=$interval->format('%i');
                                $si=$interval->format('%s');
                                $m=$mi/60;
                                $s=$si/3600;
                                $h=$hi+$s+$m;
                                $vel=$this->distanceCalculation($tr->lat,$tr->lng,$trant->lat,$trant->lng,220,$h);
                                $tr->vel=$vel;
                                $tr->save();
                            }
                            //traking
                            foreach($st as $s){
                                $sert=$s;
                                if($sert->traking_ids==""){
                                    $sert->traking_ids=$tr->id;
                                }else{
                                    $sert->traking_ids=$sert->traking_ids."||".$tr->id;
                                }
                                $sert->save();
                            }
                        }
                    }*/
                    echo json_encode(array(array("id"=>1)));
                }else{
                    echo json_encode(array(array("id"=>0)));
                }
            }else{
                /*$o=Traking::find("all",array("conditions"=>"unity_id='0' and operator='".$dri->id."' and device_id='".$p->device_id."' and lat='".$lat."' and lng='".$lon."' and created_at={ts '".date("Y-m-d H:i:s")."'}"));
                if(count($o)){
                    exit;
                }*/
                /*$tr=new Traking();
                //$lats=$lat." ";
                //$lons=$lon." ";
                //if(strlen($lats) < 14 || strlen($lons) < 14){
                //    exit;
                //}
                $tr->unity_id=$un->id;
                $tr->operator=$dri->id;
                $tr->device_id=$p->device_id;
                $tr->lat=$lat;
                $tr->lng=$lon;*/
                $dri->lat=$lat;
                $dri->lng=$lon;
                if($dri->is_valid()){
                    $dri->save();
                    /*
                    if($tr->is_valid()){
                        $st=Servicetraking::find("all",array("conditions"=>"status='1' and device_id='".$di."'","order"=>"id asc"));
                        if(count($st)){
                            $tr->save();
                            //velocidad
                            $atr=Traking::find("all",array("conditions"=>"id <> '".$tr->id."' and device_id='".$di."'","order"=>"id desc","limit"=>"1"));
                            if(count($atr)){
                                foreach($atr as $ultrack){
                                    $trant=$ultrack;
                                }
                                $datetime1 = new DateTime(date("Y-m-d H:i:s",strtotime($trant->updated_at)));
                                $datetime2 = new DateTime(date("Y-m-d H:i:s",strtotime($tr->updated_at)));
                                $interval = $datetime1->diff($datetime2);
                                $hi=$interval->format('%H');
                                $mi=$interval->format('%i');
                                $si=$interval->format('%s');
                                $m=$mi/60;
                                $s=$si/3600;
                                $h=$hi+$s+$m;
                                $vel=$this->distanceCalculation($tr->lat,$tr->lng,$trant->lat,$trant->lng,220,$h);
                                $tr->vel=$vel;
                                $tr->save();
                            }
                            //traking
                            foreach($st as $s){
                                $sert=$s;
                                if($sert->traking_ids==""){
                                    $sert->traking_ids=$tr->id;
                                }else{
                                    $sert->traking_ids=$sert->traking_ids."||".$tr->id;
                                }
                                $sert->save();
                            }
                        }
                    }*/
                    echo json_encode(array(array("id"=>1)));
                }else{
                    echo json_encode(array(array("id"=>0)));
                }
            }
        }
    }
    public function savepostim($di,$lat,$lon,$tim){
        exit;
        $tr=new Traking();
        //$lats=$lat." ";
        //$lons=$lon." ";
        //if(strlen($lats) < 14 || strlen($lons) < 14){
        //    exit;
        //}
        $tr->created_at=date("Y-m-d H:i:s",strtotime($tim));
        $tr->updated_at=date("Y-m-d H:i:s",strtotime($tim));
        $p=Phone::find_by_device_id($di);
        $un=Unity::find_by_phone_id($p->id);
        $tr->unity_id=$un->id;
        $tr->operator=$un->driver;
        $tr->device_id=$un->device_id;
        $tr->lat=$lat;
        $tr->lng=$lon;
        $un->lat=$lat;
        $un->lng=$lon;
        if($un->is_valid()){
            $un->save();
            if($tr->is_valid()){
                $st=Servicetraking::find("all",array("conditions"=>"status='1' and device_id='".$di."'"));
                if(count($st)){
                    $tr->save();
                    //velocidad
                    $atr=Traking::find("all",array("conditions"=>"id <> '".$tr->id."' and device_id='".$di."'","order"=>"id desc","limit"=>"1"));
                    if(count($atr)){
                        foreach($atr as $ultrack){
                            $trant=$ultrack;
                        }
                        $datetime1 = new DateTime(date("Y-m-d H:i:s",strtotime($trant->updated_at)));
                        $datetime2 = new DateTime(date("Y-m-d H:i:s",strtotime($tr->updated_at)));
                        $interval = $datetime1->diff($datetime2);
                        $hi=$interval->format('%H');
                        $mi=$interval->format('%i');
                        $si=$interval->format('%s');
                        $m=$mi/60;
                        $s=$si/3600;
                        $h=$hi+$s+$m;
                        $vel=$this->distanceCalculation($tr->lat,$tr->lng,$trant->lat,$trant->lng,220,$h);
                        $tr->vel=$vel;
                        $tr->save();
                    }
                    //traking
                    foreach($st as $s){
                        $sert=$s;
                        break;
                    }
                    if($sert->traking_ids==""){
                        $sert->traking_ids=$tr->id;
                    }else{
                        $sert->traking_ids=$sert->traking_ids."||".$tr->id;
                    }
                    $sert->save();
                }
            }
            echo json_encode(array(array("id"=>1)));
        }else{
            echo json_encode(array(array("id"=>0)));
        }
    }
    public function createser($id,$device){
        $as=Assignment::find_by_reservation_id($id);
        if(!$as){
            $as=Assignment::find_by_ticket_id($id);
        }
        $ns=Servicetraking::find_by_assignment_id($as->id);
        if($ns){
            exit;
        }
        $n=new Servicetraking();
        $n->start_time=date("Y-m-d G:i:s");
        $n->assignment_id=$as->id;
        $n->device_id=$device;
        $n->save();
    }
    function getRhumbLineBearing($lat1, $lon1, $lat2, $lon2) {
        //difference in longitudinal coordinates
        $dLon = deg2rad($lon2) - deg2rad($lon1);
      
        //difference in the phi of latitudinal coordinates
        $dPhi = log(tan(deg2rad($lat2) / 2 + pi() / 4) / tan(deg2rad($lat1) / 2 + pi() / 4));
      
        //we need to recalculate $dLon if it is greater than pi
        if(abs($dLon) > pi()) {
           if($dLon > 0) {
              $dLon = (2 * pi() - $dLon) * -1;
           }
           else {
              $dLon = 2 * pi() + $dLon;
           }
        }
        //return the angle, normalized
        return (rad2deg(atan2($dLon, $dPhi)) + 360) % 360;
    }
    public function posic($call="") {
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/json');

        //exit;
        $oper=Operator::find("all",array("select"=>"id,lat,lng,name,lastname,latlng,phone_id,dt,bat","conditions"=>"phone_id<>'' || phone_id<>'0'"));
        $arr=array();
        $c=0;
        $de=0;
        
        $dt=date("Y-m-d H:i:s");
        foreach($oper as $op){
            $hr=0; 
            $ca=0;
            $ser=Assignment::find("all",array("select"=>"reservation_id,ticket_id,updated_at","conditions"=>"operator_id='".$op->id."'","limit"=>"3","order"=>"id desc"));
            
            foreach($ser as $se){
                if($se->reservation_id){
                    $reser=Reservation::find($se->reservation_id);
                }else{
                    $reser=Ticket::find($se->ticket_id);
                }
                if($reser->status==2){
                    $dt2=date("Y-m-d H:i:s",strtotime($se->updated_at));
                    $date1=date_create($dt2);
                    $date2=date_create($dt);
                    $diff=date_diff($date1,$date2);
                    if($diff->format('%h')>=2){
                        $hr=1;
                    }
                    
                    $ca++;
                }
            }
            
            $un=Unity::find("all",array("select"=>"created_at,status,driver,economic,status","conditions"=>"driver='".$op->id."'"));
             
            if($un){
                if($op->lat==0){
                    continue;
                }
                $un=Unity::find_by_driver($op->id,array("select"=>"created_at,status,driver,economic,status"));
               
                
                if($un->driver!=""){
                    $nm=$op->name." ".$op->lastname;
                }else{
                    $nm="";
                }
                
                $arr[$c]["id"]=$un->economic;
                $dev=Phone::find($op->phone_id);

                $arr[$c]["nombre"]=$nm;
                $arr[$c]["numero"]=$dev->number;
                $con=1;
                $arr[$c]["lat"]=$op->lat;
                $arr[$c]["bat"]=$op->bat;
                $arr[$c]["ver"]=0;

                if($dev->version==15 || $dev->version=="15"){
                    $arr[$c]["ver"]=1;
                }
                $arr[$c]["lng"]=$op->lng;
                $arr[$c]["fecha"]=date("Y-m-d H:i:s",strtotime($op->dt));
                $ex=explode("||",$op->latlng);
                $arr[$c]["rot"]=$this->getRhumbLineBearing($ex[0],$ex[1],$op->lat,$op->lng); 
                if($ca==0){
                    $un->status=1;
                }else if($ca==1){
                    $un->status=2;
                }else{
                    $un->status=3;
                }
                if($hr>=1){
                    $un->status=4;
                }
                
                if($op->dt==""){
                    $un->status=0;
                }else{
                    $dar=date("Y-m-d H:i:s");
                    $d1=strtotime($op->dt)."-";
                    $tsdar=strtotime($dar);
                    $lim=$tsdar-$d1;
                   
                    if($lim<0){
                        $lim=$d1-$tsdar;
                    }
                    if($lim>600){
                        $un->status=0;
                    }
                }
                $arr[$c]["status"]=$un->status;

                $c++;
            }
        }
        echo "yeijs(".json_encode($arr).")";
    }
    public function getser($inicio,$fin,$unidad,$operador) {

        if(!isset($fin) || $fin==" "){
            $fin=date("Y-m-d H:i:s");
        }
        $cr="";
        if(isset($inicio) && $inicio!="" && $inicio!=" "){
            $ini=date("Y-m-d",strtotime($inicio))." 00:00:00";
            $cr="(created_at BETWEEN {ts '".$ini."'} and {ts '".$fin."'})";
        }
        if($cr!="" && ($unidad!=" " || $operador!=" ")){
            if($unidad!=" "){
                $cr=$cr." and unity_id='".$unidad."'";
            }
            if($operador!=" "){
                $cr=$cr." and operator_id='".$operador."'";
            }
        }else if($unidad!=" " || $operador!=" "){
            if($unidad!=" "){
                $cr="unity_id='".$unidad."'";
            }
            if($unidad!=" " && $operador!=" "){
                $cr=$cr." and operator_id='".$operador."'";
            }else{
                if($operador!=" ")
                $cr=$cr."operator_id='".$operador."'";
            }
        }
        echo "<table class='table table-bordered'>";
        echo "<thead>";
        echo "<tr>
        <th>#</th>
        <th>Service</th>
        <th>Operador</th>
        <th>Unidad</th>
        <th><button class='btn btn-success'></button></th>
        </tr>";
        echo "</thead><tbody>";
        $ass=Assignment::find("all",array("conditions"=>$cr,"select"=>"id,ticket_id,reservation_id,unity_id,operator_id"));
        if(count($ass)){
            $c=1;
            foreach($ass as $as){
                $serv=Servicetraking::find_by_assignment_id($as->id,array("select"=>"id"));
                if(!count($serv)){
                    continue;
                }
                $s=Service::find_by_reservation1($as->reservation_id);
                if(!$s){
                    $s=Service::find_by_reservation2($as->reservation_id);
                }else{
                    $sd=$s->id;
                }
                if(!$s){
                    $s=Ticket::find_by_id($as->ticket_id);
                    $sd=$s->code;
                }else{
                    $sd=$s->id;
                }
                $un=Unity::find($as->unity_id);
                $op=Operator::find($as->operator_id);
                echo "<tr>
                <td>".$c."</td>
                <td>".$sd."</td>
                <td>".$op->name." ".$op->lastname."</td>
                <td>".$un->economic."</td>
                <td><button class='vermapa btn btn-success' data-id='".$as->id."'>Ver Mapa</button></td>
                </tr>";
                $c++;
                if($c>15){
                    break;
                }
            }
        }
        echo "</tbody></table>";
    }
    public function getexceso($inicio,$fin,$unidad,$operador) {
        if(!isset($fin) || $fin==" "){
            $fin=date("Y-m-d H:i:s");
        }else{
            $fin=date("Y-m-d",strtotime($fin))." 00:00:00";
        }
        $cr1="";
        if(isset($inicio) && $inicio!="" && $inicio!=" "){
            $ini=date("Y-m-d",strtotime($inicio))." 00:00:00";
            $cr1="(created_at BETWEEN {ts '".$ini."'} and {ts '".$fin."'})";
            $cr3="(created_at BETWEEN {ts '".$ini."'} and {ts '".$fin."'})";
        }
        if($cr!="" && ($unidad!=" " || $operador!=" ")){
            if($unidad!=" "){
                $un=Unity::find_by_economic($unidad);
                $unidad=$un->id;
                $cr1=$cr1." and unity_id='".$unidad."'";
            }
            if($operador!=" "){
                $cr1=$cr1." and operator_id='".$operador."'";
            }
        }else if($unidad!=" " || $operador!=" "){
            
            if($unidad!=" "){
                $un=Unity::find_by_economic($unidad);
                $unidad=$un->id;
                $cr1=$cr1." and unity_id='".$unidad."'";
            }
            if($unidad!=" " && $operador!=" "){
                $cr1=$cr1." and operator='".$operador."'";
            }else{
                if($operador!=" ")
                $cr1=$cr1." and operator='".$operador."'";
            }
        }

        if($cr1==""){
            $cr1=$cr1."vel > 28.8888888888912";
        }else{
            $cr1=$cr1." and vel > 28.8888888888912";
        }
        $ass1=Traking::find("all",array("conditions"=>$cr1,"group"=>"operator","order"=>"operator asc,id asc"));
        if(count($ass1)){
            echo "<table class='table table-bordered'>";
                echo "<thead>";
                echo "<tr>
                <th></th>
                <th>Operador</th>
                </tr>";
                echo "</thead><tbody>";
            foreach($ass1 as $as1){
                $uni=Unity::find($as1->unity_id,array("select"=>"id,economic,driver"));
                $ope=Operator::find($as1->operator,array("select"=>"id,name,lastname"));
                echo "<td><button class='btn btn-xs btn-primary op' data-id='".$ope->id."'>Ver</button></td>";
                echo "<td>".$ope->name." ".$ope->lastname."</td></tr>";
                echo "<tr><td colspan='2'>";
                echo "<table class='table table-bordered tb".$ope->id."' style='display:none'>";
                echo "<thead>";
                echo "<tr>
                <th>Fecha</th>
                <th>Velocidad</th>
                <th>Unidad</th>
                <th>Operador</th>
                <th></th>
                <th></th>
                </tr>";
                echo "</thead><tbody>";
                $c=1;
                
                $cr2=$cr3;
                if($unidad!=" "){
                    $cr2=$cr2." and unity_id='".$unidad."' and operator='".$as1->operator."'";
                }else{
                    $cr2=$cr2." and operator='".$as1->operator."'";
                }
                $cr2=$cr2." and vel > 28.8888888888912";
                $cant=0;
                $ass=Traking::find("all",array("conditions"=>$cr2,"order"=>"id asc"));
                $opr="";
                foreach($ass as $as){
                    if($opr!=$as->operator){
                        $opr=$as->operator;
                        $uni=Unity::find($as->unity_id,array("select"=>"id,economic,driver"));
                        $ope=Operator::find($as->operator,array("select"=>"id,name,lastname"));
                        $as->vel=($as->vel*3.6);
                        if($cant!=0){
                            echo "<td><button class='vermapa btn btn-success' data-id='".$idi."-".$idf."-".$as->operator."'>Ver en Mapa</button></td>
                            <td><a target='_blank' href='http://resv.goldenmty.com/home/descargar/".$idi."/".$idf."/".$as->operator."'><button class='btn btn-success'>Descargar</button></a></td></tr>";
                        }
                        $idi=$as->id;
                        $idf=$as->id;
                        echo "<tr>
                        <td>".date("Y-m-d H:i",strtotime($as->created_at))."</td>
                        <td>".$as->vel."</td>
                        <td>".$uni->economic."</td>
                        <td>".$ope->name." ".$ope->lastname."</td>";
                        $cant++;
                        
                    }else{
                        $fe1=strtotime($as->created_at);
                        $fe2=strtotime($fe);
                        if($fe1>=$fe2){
                            $resu=$fe1-$fe2;
                        }else{
                            $resu=$fe2-$fe1;
                        }
                        if($resu>300){
                            $as->vel=($as->vel*3.6);
                            echo "<td><button class='vermapa btn btn-success' data-id='".$idi."-".$idf."-".$as->operator."'>Ver en Mapa</button></td>
                            <td><a target='_blank' href='http://resv.goldenmty.com/home/descargar/".$idi."/".$idf."/".$as->operator."'><button class='btn btn-success'>Descargar</button></a></td></tr>";
                            $idi=$as->id;
                            $idf=$as->id;
                            echo "<tr>
                            <td>".date("Y-m-d H:i",strtotime($as->created_at))."</td>
                            <td>".$as->vel."</td>
                            <td>".$uni->economic."</td>
                            <td>".$ope->name." ".$ope->lastname."</td>";
                        }else{
                            $idf=$as->id;
                        }
                    }
                    $fe=$as->created_at;
                }
                echo "<td><button class='vermapa btn btn-success' data-id='".$idi."-".$idf."-".$as->operator."'>Ver en Mapa</button></td>
                <td><a target='_blank' href='http://resv.goldenmty.com/home/descargar/".$idi."/".$idf."/".$as->operator."'><button class='btn btn-success'>Descargar</button></a></td></tr>";
                echo "</tbody></table>";
                echo "</td></tr>";
            }
            echo "</tbody></table>";   
        }
    }

    function getroute($direccion){
        //direccion a buscar
        $direccion= urlencode($direccion);
        return "0,0";

        exit;
        //Buscamos la direccion en el servicio de google
        $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$direccion.'&sensor=false');

        //decodificamos lo que devuelve google, que esta en formato json
        $output= json_decode($geocode);

        //Extraemos la informacion que nos interesa
        $lat = $output->results[0]->geometry->location->lat;
        $long = $output->results[0]->geometry->location->lng;

        return "".$lat.",".$long;
        //la imprimimos
    }
    function getpoints($asig){
        $as=Servicetraking::find_by_assignment_id($asig);
        $ex=explode("||",$as->traking_ids);
        $ar=array();
        $c=0;
        $r=0;
        foreach($ex as $e){
            if(count(Traking::find("all",array("conditions"=>"id='".$e."'","select"=>"id")))){
                if($r==0){
                    $tr=Traking::find($e);
                    $ar[$c]["lat"]=$tr->lat;
                    $ar[$c]["lng"]=$tr->lng;
                    $ar[$c]["date"]=date("Y-m-d H:i:s",strtotime($tr->created_at));
                    $c++;
                   
                    
                }
            }
            
        }
        //header('Content-Type: application/json');
        echo "yeijs(".json_encode($ar).")";
    }
    function getpexceso($asig){
        $ar=array();
        $ini=explode("-",$asig);
        $as=Traking::find("all",array("select"=>"id","limit"=>3,"conditions"=>"id<".$ini[0]." and operator='".$ini[2]."'","order"=>"id desc"));
       
        foreach($as as $a){
            $inicial=$a->id;
        }
        $as=Traking::find("all",array("select"=>"id","limit"=>3,"conditions"=>"id > ".$ini[1]." and operator='".$ini[2]."'","order"=>"id asc"));
        foreach($as as $a){
            $final=$a->id;
        }
        $dtra=Traking::find("all",array("conditions"=>"id>".$inicial." and id<".$final." and operator='".$ini[2]."'","order"=>"id desc"));
        $c=0;
        foreach($dtra as $tr){
            $ar[$c]["id"]=$tr->id;
            $ar[$c]["lat"]=$tr->lat;
            $ar[$c]["lng"]=$tr->lng;
            $ar[$c]["date"]=date("Y-m-d H:i:s",strtotime($tr->created_at));
            $c++;
        }
        header('Content-Type: application/json');
        echo "yeijs(".json_encode($ar).")";
    }
    function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $decimals = 2,$tiempo) {
        // Cálculo de la distancia en grados
        $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));

        // Conversión de la distancia en grados a la unidad escogida (kilómetros, millas o millas naúticas)

        $distance = $degrees * 111.13384; // 1 grado = 111.13384 km, basándose en el diametro promedio de la Tierra (12.735 km)
        if($tiempo){
        $vel=($distance/$tiempo);
            return round($vel, $decimals);
        }else{
            return 0;
        }
    }
    function prueva(){
        $trant=Traking::find(617);
        $tr=Traking::find(615);

    }
    public function settoken($dev_id,$token){
        $ph=Phone::find("all",array("conditions"=>"device_id='".$dev_id."'"));
        if(count($ph)){
            $ph=Phone::find_by_device_id($dev_id);
            $ph->token=$token;
            $ph->save();
        }else{
            $ph = new Phone();
            $ph->device_id=$dev_id;
            $ph->token=$token;
            $ph->save();
        }
        echo json_encode(array("1"));
    }
    public function transfecha($fec){
        $fe=date("D",strtotime($fec));
        switch($fe){
            case 'Mon':
                $d="Lun";
                break;
            case 'Tue':
                $d="Mar";
                break;
            case 'Wed':
                $d="Mie";
                break;
            case 'Thu':
                $d="Jue";
                break;
            case 'Fri':
                $d="Vie";
                break;
            case 'Sat':
                $d="Sab";
                break;
            case 'Sun':
                $d="Dom";
                break;
        }
        $fe=date("n",strtotime($fec));
        switch($fe){
            case '1':
                $m="Ene";
                break;
            case '2':
                $m="Feb";
                break;
            case '3':
                $m="Mar";
                break;
            case '4':
                $m="Abr";
                break;
            case '5':
                $m="May";
                break;
            case '6':
                $m="Jun";
                break;
            case '7':
                $m="Jul";
                break;
            case '8':
                $m="Ago";
                break;
            case '9':
                $m="Sep";
                break;
            case '10':
                $m="Oct";
                break;
            case '11':
                $m="Nov";
                break;
            case '12':
                $m="Dic";
                break;
        }
        $dn=date("d",strtotime($fec));
        
        return $d." ".$dn." ".$m;
    }
}