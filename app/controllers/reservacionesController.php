<?php
class reservacionesController extends controllerBase {
    public function index(){
        
    }
    public function getreser($type, $status){
        $hoy = date("Y-m-d");
		$hora = date("G:i");
		if (strtotime($hora) < strtotime(date("G:i", strtotime("4:00"))) && strtotime($hora) > strtotime(date("G:i", strtotime("00:00")))) {
			$ayer = strtotime('-1 day', strtotime($hoy));
			$ayer = date('Y-m-d', $ayer);
			$reser=Reservation::find_by_sql("SELECT * FROM `reservations` WHERE type='".$type."' AND status=".$status." AND (reservation_date = {ts '" . $ayer . "'} OR reservation_date = {ts '" . $hoy . "'}) ORDER BY reservation_date asc, reservation_time asc");
		} else {
			$reser=Reservation::find_by_sql("SELECT * FROM `reservations` WHERE type='".$type."' AND status=".$status." AND reservation_date = {ts '" . $hoy . "'} ORDER BY reservation_date asc, reservation_time asc");
		}
		$arr=array();
		foreach($reser as $re){
		    $ar=array();
		    $s=Service::find_by_sql("SELECT id,user_id,reservation1,reservation2,payment FROM `services` WHERE reservation1 = '".$re->id."' OR reservation2 = '".$re->id."'")[0];
		    $user=User::find_by_sql("SELECT * FROM `users` WHERE id = '".$s->user_id."'")[0];
		    $emp=User::find_by_sql("SELECT * FROM `users` WHERE id = '".$user->business_id."'")[0];
			if(!$emp){
				$emp=User::find_by_id("SELECT * FROM `users` WHERE id = '".$user->business_id."'")[0];
			}
			if($s->reservation2){
				if($s->reservation2 == $re->id){
					$ar['otipo'] = 'COM';
				}else{
					$ar['otipo'] = 'RED';
				}
			} else {
				$ar['otipo'] = 'SEN';
			}
			$ar['id'] = $re->id;
			$ar['update'] = date("Y-m-d G:i",strtotime($re->updated_at));
			$ar['empresa'] = $emp->name;
		    $ar['fpago'] = $s->payment;
		    $ar['id_user'] = $user->id;
		    $ar['nombre'] = $user->name;
		    $ar['service_id'] = $s->id;
		    $ar['tipo'] = $re->type;
		    $ar['fecha'] = date("Y-m-d",strtotime($re->reservation_date));
		    $ar['hora'] = date("H:i",strtotime($re->reservation_time));
			$ar['vuelo'] = $re->flight_number;
			if($re->vehicle_id == 1){
				$ar['vehiculo'] = 'Auto';
			} else {
				$ar['vehiculo'] = 'Cam';
			}
		    $ar['pasajeros'] = $re->passengers;
		    $ar['terminal'] = $re->terminal;
		    $value = $re->addresses;
		    $lon=strlen($value);
			$lon=($lon-1)*(-1);
			$ind=substr($value, 0,$lon);
			if($ind == "a"){
    			$value=str_replace("a", "", $value);
    			$dir=Address::find_by_id($value);
    			$sub=Suburb::find_by_id($dir->suburb_id);
    			if($cond){
    				$ar['direccion'] = "{$sub->suburb}";
    			}else{
    			    $ar['direccion'] = "Colonia {$sub->suburb}";
			    }
			}else if($ind == "s"){
				$value=str_replace("s", "", $value);
				$sub=Suburb::find_by_id($value);
				if($cond){
				    $ar['direccion'] = "{$sub->suburb}";
			    }else
				    $ar['direccion'] = "{$sub->suburb}";
			}
		    array_push($arr,$ar);
		}
		echo json_encode($arr);
	}
}
