<?php
class reportesController extends controllerBase {
    public function index(){
        
	}
	public function MTH(){
        if( authDriver::isLoggedin() ) {
            $this->getView()->mth();
        } else {
            $this->getView()->index();
        }
	}
	public function CLV(){
        if( authDriver::isLoggedin() ) {
            $this->getView()->clv();
        } else {
            $this->getView()->index();
        }
	}
	

	public function clvplantilla($ano){
		$txt = "";
		$inp = "";
		$j = true;
		$t = 0;
		$arr = array();
		$arra = array();
		$arra[0] = array("nombre" => "ADMINISTRATIVO", "clase" => "adm");
		$arra[1] = array("nombre" => "OPERATIVO", "clase" => "ope");
		$arra[2] = array("nombre" => "OPERATIVO TIEMPO EXTRA", "clase" => "opex");
		$adm1 =  (array) json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='adm' and (planta='CLVC' OR planta = 'CLVE')"))[0]->valores);
		$ope1 =  (array)  json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='ope' and (planta='CLVC' OR planta = 'CLVE')"))[0]->valores);
		$opex1 =  (array)  json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='opex' and (planta='CLVC' OR planta = 'CLVE')"))[0]->valores);
		$adm2 =  (array) json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='adm' and (planta='CLVC' OR planta = 'CLVE')"))[1]->valores);
		$ope2 =  (array)  json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='ope' and (planta='CLVC' OR planta = 'CLVE')"))[1]->valores);
		$opex2 =  (array)  json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='opex' and (planta='CLVC' OR planta = 'CLVE')"))[1]->valores);
		?>
		<div class="row" style="width:100%">
			<div id="accordion" style="width:100%">
		<?php
		$c=0;
		foreach($arra as $a){
			?>
			<div class="card">
				<div class="card-header" id="headingOne">
					<h5 class="mb-0">
						<button class="btn btn-link" data-toggle="collapse" data-target="#table<?php echo $c;?>" aria-expanded="false" aria-controls="table<?php echo $c;?>">
						<?php echo $a["nombre"]; ?>
						</button>
					</h5>
				</div>
				<div id="table<?php echo $c;?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
					<div class="card-body">

			<?php
			echo "<table class='table table-hover'>
					<tr>
						<td></td>";
			for($i = 1; $i <= 31; $i++){
				echo "<td>" . $i . "</td>";
			}
			echo "</tr>";
			for($i = 1; $i <= 12; $i++){
				echo "<tr>";
				$d = cal_days_in_month(CAL_GREGORIAN, $i, $ano);
				$da = date("M",strtotime("2020-".$i."-01"));
				echo "<td>".$da."</td>";
				for($j = 1; $j <= $d; $j++){
					
					if($a["clase"]=='adm'){
						$vale = $adm1[$i."-".$j]+$adm2[$i."-".$j];

					}else if($a["clase"]=='ope'){
						$vale = $ope1[$i."-".$j]+$ope2[$i."-".$j];

					}else if($a["clase"]=='opex'){
						$vale = $opex1[$i."-".$j]+$opex2[$i."-".$j];

					}
					if($vale == "" ){
						$vale = '0';
					}
					
					echo "<td><input type='number' style='padding-left:0px;padding-right:0px;' min='0' class='form-control ".$a["clase"]."' name='".$i."-".$j."' value='".$vale."' /></td>";
				}
				echo "</tr>";
			}
			echo "</table>";
			echo "</div>
			</div>
		</div>";
		$c++;
		}
		
		/*foreach($arra as $a){
			echo "<div class='row'>";
			echo "<div class='row'><h3>".$a["nombre"]."</h3></div>";
			for($i = 1; $i <= $d; $i++){
				echo "<div class='col-xs-1 no-padding'>
					<div class='col-xs-12'>" . $i . "</div>
					<div class='col-xs-12 no-padding'><input class='form-control inp' name='".$a["clase"].$i."' value='0' /></div>
				</div>";
			}
			echo "</div>";
		}*/
		echo "</div>
		</div>
		<br /><br /><button class='btn btn-success btn-lg btn-guardar'>Guardar</button>";
	}
	public function mthplantilla($ano){
		$txt = "";
		$inp = "";
		$j = true;
		$t = 0;
		$arr = array();
		$arra = array();
		$arra[0] = array("nombre" => "ADMINISTRATIVO", "clase" => "adm");
		$arra[1] = array("nombre" => "OPERATIVO", "clase" => "ope");
		$arra[2] = array("nombre" => "OPERATIVO TIEMPO EXTRA", "clase" => "opex");
		$adm1 =  (array) json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='adm' and (planta='ASSY' OR planta = 'C&C')"))[0]->valores);
		$ope1 =  (array)  json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='ope' and (planta='ASSY' OR planta = 'C&C')"))[0]->valores);
		$opex1 =  (array)  json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='opex' and (planta='ASSY' OR planta = 'C&C')"))[0]->valores);
		$adm2 =  (array) json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='adm' and (planta='ASSY' OR planta = 'C&C')"))[1]->valores);
		$ope2 =  (array)  json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='ope' and (planta='ASSY' OR planta = 'C&C')"))[1]->valores);
		$opex2 =  (array)  json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='opex' and (planta='ASSY' OR planta = 'C&C')"))[1]->valores);
		?>
		<div class="row" style="width:100%">
			<div id="accordion" style="width:100%">
		<?php
		$c=0;
		foreach($arra as $a){
			?>
			<div class="card">
				<div class="card-header" id="headingOne">
					<h5 class="mb-0">
						<button class="btn btn-link" data-toggle="collapse" data-target="#table<?php echo $c;?>" aria-expanded="false" aria-controls="table<?php echo $c;?>">
						<?php echo $a["nombre"]; ?>
						</button>
					</h5>
				</div>
				<div id="table<?php echo $c;?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
					<div class="card-body">

			<?php
			echo "<table class='table table-hover'>
					<tr>
						<td></td>";
			for($i = 1; $i <= 31; $i++){
				echo "<td>" . $i . "</td>";
			}
			echo "</tr>";
			for($i = 1; $i <= 12; $i++){
				echo "<tr>";
				$d = cal_days_in_month(CAL_GREGORIAN, $i, $ano);
				$da = date("M",strtotime("2020-".$i."-01"));
				echo "<td>".$da."</td>";
				for($j = 1; $j <= $d; $j++){
					
					if($a["clase"]=='adm'){
						$vale = $adm1[$i."-".$j]+$adm2[$i."-".$j];

					}else if($a["clase"]=='ope'){
						$vale = $ope1[$i."-".$j]+$ope2[$i."-".$j];

					}else if($a["clase"]=='opex'){
						$vale = $opex1[$i."-".$j]+$opex2[$i."-".$j];

					}
					if($vale == "" ){
						$vale = '0';
					}
					
					echo "<td><input type='number' style='padding-left:0px;padding-right:0px;' min='0' class='form-control ".$a["clase"]."' name='".$i."-".$j."' value='".$vale."' /></td>";
				}
				echo "</tr>";
			}
			echo "</table>";
			echo "</div>
			</div>
		</div>";
		$c++;
		}
		
		/*foreach($arra as $a){
			echo "<div class='row'>";
			echo "<div class='row'><h3>".$a["nombre"]."</h3></div>";
			for($i = 1; $i <= $d; $i++){
				echo "<div class='col-xs-1 no-padding'>
					<div class='col-xs-12'>" . $i . "</div>
					<div class='col-xs-12 no-padding'><input class='form-control inp' name='".$a["clase"].$i."' value='0' /></div>
				</div>";
			}
			echo "</div>";
		}*/
		echo "</div>
		</div>
		<br /><br /><button class='btn btn-success btn-lg btn-guardar'>Guardar</button>";
	}
	public function extraplantilla($f){
		?>
		<div class="row" style="width:100%">
			<div id="accordion" style="width:100%">
			<?php
			$a = Extratime::find('all', array('conditions'=>'fecha="'.$f.'" and (type="ope")'));
			$arr = array();
			foreach($a as $c){
				$b = json_decode($c->valores, true);
				$arr[$c->planta] = $b;
			}
			
			$arr['MTH-CTTL'] = array();
			$arr['CLVTTL'] = array();
			$arr['PLANTATTL'] = array();
			$arr['ARNESTTL'] = array();
			$arr['K&STTL'] = array();
			foreach($arr as $k => $ar){
				foreach($ar as $k2 => $a){
					if(!isset($arr['MTH-CTTL'][$k2])){
						$arr['MTH-CTTL'][$k2] = 0;
						$arr['PLANTATTL'][$k2] = 0;
						$arr['ARNESTTL'][$k2] = 0;
						$arr['CLVTTL'][$k2] = 0;	
						$arr['K&STTL'][$k2] = 0;	
					}
					if($k == "MTH-CC&C" || $k == "MTH-CASSY"){
						$arr['MTH-CTTL'][$k2] += $arr[$k][$k2];
						$arr['PLANTATTL'][$k2] += $arr[$k][$k2];
						$arr['ARNESTTL'][$k2] += $arr[$k][$k2];
						$arr['K&STTL'][$k2] += $arr[$k][$k2];

					}
					if($k == "CLVC&C" || $k == "CLVASSY"){
						$arr['CLVTTL'][$k2] += $arr[$k][$k2];
						$arr['PLANTATTL'][$k2] += $arr[$k][$k2];
						$arr['ARNESTTL'][$k2] += $arr[$k][$k2];
						$arr['K&STTL'][$k2] += $arr[$k][$k2];

					}
					if($k == "JER" || $k == "AGS" || $k == "SDH" || $k == "PSC" || $k == "SFP"){
						$arr['PLANTATTL'][$k2] += $arr[$k][$k2];
						$arr['ARNESTTL'][$k2] += $arr[$k][$k2];
						$arr['K&STTL'][$k2] += $arr[$k][$k2];

					}
					if($k == "CORP"){
						$arr['ARNESTTL'][$k2] += $arr[$k][$k2];
						$arr['K&STTL'][$k2] += $arr[$k][$k2];

					}
					if($k == "CABLE" || $k == "ELE" || $k == "EPB"){
						$arr['K&STTL'][$k2] += $arr[$k][$k2];
					}
				}
			}
			$e = Extratime::find('all', array('conditions'=>'fecha="'.$f.'" and (type="opex")'));
			$arr3 = array();
			foreach($e as $f){
				$g = json_decode($f->valores, true);
				$arr3[$f->planta] = $g;
			}
			$arr2 = array();
			foreach($arr3 as $k => $ar2){
				$arr2[$k]=array();
				foreach($ar2 as $k2 => $a2){
					if($arr3[$k][$k2]){
						$va=$arr[$k][$k2]/$arr3[$k][$k2];
					}else{
						$va=0;
					}
					$arr2[$k][$k2]=$va;
				}
			}
			$arr2['MTH-CTTL'] = array();
			$arr2['CLVTTL'] = array();
			$arr2['PLANTATTL'] = array();
			$arr2['ARNESTTL'] = array();
			$arr2['K&STTL'] = array();
			foreach($arr2 as $k => $ar){
				foreach($ar as $k2 => $a){
					if(!isset($arr2['MTH-CTTL'][$k2])){
						$arr2['MTH-CTTL'][$k2] = 0;
						$arr2['PLANTATTL'][$k2] = 0;
						$arr2['ARNESTTL'][$k2] = 0;
						$arr2['CLVTTL'][$k2] = 0;	
						$arr2['K&STTL'][$k2] = 0;	
					}
					if($k == "MTH-CC&C" || $k == "MTH-CASSY"){
						$arr2['MTH-CTTL'][$k2] += $arr2[$k][$k2];
						$arr2['PLANTATTL'][$k2] += $arr2[$k][$k2];
						$arr2['ARNESTTL'][$k2] += $arr2[$k][$k2];
						$arr2['K&STTL'][$k2] += $arr2[$k][$k2];

					}
					if($k == "CLVC&C" || $k == "CLVASSY"){
						$arr2['CLVTTL'][$k2] += $arr2[$k][$k2];
						$arr2['PLANTATTL'][$k2] += $arr2[$k][$k2];
						$arr2['ARNESTTL'][$k2] += $arr2[$k][$k2];
						$arr2['K&STTL'][$k2] += $arr2[$k][$k2];

					}
					if($k == "JER" || $k == "AGS" || $k == "SDH" || $k == "PSC" || $k == "SFP"){
						$arr2['PLANTATTL'][$k2] += $arr2[$k][$k2];
						$arr2['ARNESTTL'][$k2] += $arr2[$k][$k2];
						$arr2['K&STTL'][$k2] += $arr2[$k][$k2];

					}
					if($k == "CORP"){
						$arr2['ARNESTTL'][$k2] += $arr2[$k][$k2];
						$arr2['K&STTL'][$k2] += $arr2[$k][$k2];

					}
					if($k == "CABLE" || $k == "ELE" || $k == "EPB"){
						$arr2['K&STTL'][$k2] += $arr2[$k][$k2];
					}
				}
			}
			for($c=1; $c<=12; $c++){
				$month = date("F",strtotime('01-'.$c.'-2020'));
				?>
				<div class="card">
					<div class="card-header" id="headingOne">
						<h5 class="mb-0">
							<button class="btn btn-link" data-toggle="collapse" data-target="#extra<?php echo $c;?>" aria-expanded="false" aria-controls="extra<?php echo $c;?>">
							<?php echo $month; ?>
							</button>
						</h5>
					</div>
					<div id="extra<?php echo $c;?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body">
						<?php
						echo "<table class='table table-bordered'>
								<tr>
									<td colspan='6'></td>
								";
								
								for($i=1;$i<=31; $i++){
									if($i<10){
										echo "<td>0".$i."</td>";
									}else
									echo "<td>".$i."</td>";
								}
								echo "</tr>
								<tr>
									<td rowspan='38'>E<br />n<br />e<br />r<br />o</td>
									<td rowspan='18'>P<br />L<br />A<br />N<br />T<br />I<br />L<br />L<br />A</td>
									<td rowspan='13'></td>
									<td colspan='3'>CORP</td>";

									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['CORP'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td rowspan='12'>P<br />L<br />A<br />N<br />T<br />A</td>
									<td colspan='2'>AGS</td>
									";
									
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['AGS'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='2'>SDH</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['SDH'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='2'>PSC</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['PSC'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='2'>SFP</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['SFP'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='2'>JER</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['JER'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td rowspan='3'>MTH-C</td>
									<td>C&C</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['MTH-CC&C'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td>ASSY</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['MTH-CASSY'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td>TTL</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['MTH-CTTL'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td rowspan='3'>CLV</td>
									<td>C&C</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['CLVC&C'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td>ASSY</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['CLVASSY'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td>TTL</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['CLVTTL'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='2'>TTL</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['PLANTATTL'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='4'>ARNES TTL</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['ARNESTTL'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='4'>CABLE</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['CABLE'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='4'>ELE</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['ELE'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='4'>EPB</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['EPB'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='4'>K&S TTL</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr['K&STTL'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td rowspan='18'>T.<br />E.<br /><br /><br />H<br />R<br />S.<br />/<br />O<br />P<br />1</td>
									<td rowspan='13'></td>
									<td colspan='3'>CORP</td>";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['CORP'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td rowspan='12'>P<br />L<br />A<br />N<br />T<br />A</td>
									<td colspan='2'>AGS</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['AGS'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='2'>SDH</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['SDH'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='2'>PSC</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['PSC'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='2'>SFP</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['SFP'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='2'>JER</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['JER'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td rowspan='3'>MTH-C</td>
									<td>C&C</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['MTH-CC&C'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td>ASSY</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['MTH-CASSY'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td>TTL</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['MTH-CTTL'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td rowspan='3'>CLV</td>
									<td>C&C</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['CLVC&C'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td>ASSY</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['CLVASSY'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td>TTL</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['CLVTTL'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='2'>TTL</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['PLANTATTL'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='4'>ARNES TTL</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['ARNESTTL'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='4'>CABLE</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['CABLE'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='4'>ELE</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['ELE'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='4'>EPB</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['EPB'][$c."-".$i]."</td>";
									}
									echo "</tr>
									<tr>
									<td colspan='4'>K&S TTL</td>
									";
									for($i=1;$i<=31; $i++){
										echo "<td class='d".$i."'>".$arr2['K&STTL'][$c."-".$i]."</td>";
									}
									echo "</tr>";
						echo "</table>";
						?>
						</div>
					</div>
				</div>
			<?php
			}
		?>
		</div>
	</div>
	<?php
	}

	public function extra(){
		if( authDriver::isLoggedin() ) {
            $this->getView()->extra();
        } else {
            $this->getView()->index();
        }
	}
	public function faltas(){
        if( authDriver::isLoggedin() ) {
            $this->getView()->faltas();
        } else {
            $this->getView()->index();
        }
	}
	public function personal(){
        if( authDriver::isLoggedin() ) {
            $this->getView()->personal();
        } else {
            $this->getView()->index();
        }
    }
    public function rotacion(){
        
    }
}
