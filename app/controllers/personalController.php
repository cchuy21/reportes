<?php
class personalController extends controllerBase {
	public function index(){
		if( authDriver::isLoggedin() ) {
            $this->getView()->controlPanel();
        } else {
            $this->getView()->index();
        }
	}
	public function get(){
		
		$u = authDriver::getUser();
		$d = Personal::find("all", array("conditions"=>"ano='".$_POST['an']."' and mes='".$_POST['mes']."' and planta='".$u->planta."'"))[0];
		if($d){
			echo json_encode(json_decode($d->valores));
		}else{
			echo json_encode(0);
		}
	}	
	public function plantilla($mes,$ano){

		?>
		<div class="row" style="width:100%">
			<div id="accordion" style="width:100%">
				<div class="card">
					<div class="card-header" id="headingOne">
						<h5 class="mb-0">
							<button class="btn btn-link" data-toggle="collapse" data-target="#table3" aria-expanded="false" aria-controls="table3">
								0.-Detalle de Casa Matriz (*)
							</button>
						</h5>
					</div>
					<div id="table3" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body">
							<table class='table table-bordered' style='margin-left:20px;font-size: 12px;'>
								<tbody>
									<tr>
										<td rowspan='13'>Adm. general</td>
										<td>Recursos Humanos</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Servicios Generales</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Planeación</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Traductores</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Contabilidad</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Sistemas</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Ventas</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Compras</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Electronicos</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Planta Cables</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Planta Santa clara</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Director General</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td style='text-align:right'>Sub TTL</td>
										<td class="ttl1"></td>
									</tr>
									<tr>
										<td colspan="2">P.C.</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td rowspan="3">M.C.</td>
										<td>Control de Material</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Comercio Exterior</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td style='text-align:right'>Sub TTL</td>
										<td class="ttl2"></td>
									</tr>
									<tr>
										<td colspan='2'>MFG de Corporativo</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td rowspan='5'>Inge.</td>
										<td>Producción Inge.</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Mantenimiento</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Pro. Diseño</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Toluca</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td style='text-align:right'>Sub TTL</td>
										<td class="ttl3"></td>
									</tr>
									<tr>
										<td colspan="2">Calidad</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td colspan="2" style='text-align:right'>Sub TTL</td>
										<td class="ttl4"></td>
									</tr>
									<tr>
										<td colspan="2"></td>
										<td class="ttl5"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingOne">
						<h5 class="mb-0">
							<button class="btn btn-link" data-toggle="collapse" data-target="#table6" aria-expanded="false" aria-controls="table6">
								0.-Detalles de Operarios
							</button>
						</h5>
					</div>
					<div id="table6" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body">
							<table class='table table-bordered' style='margin-left:20px;font-size: 12px;'>
								<tbody>
									<tr>
										<td rowspan='4'>
											Relaciones Industriales
										</td>
										<td>Serv. Grales</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Rec. Humanos</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Capacitación</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Compras</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td colspan="2">Ctrol Prod'n (APT)</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td rowspan="2">M.C.</td>
										<td>Almacén</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Comercio Ext.</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td colspan="2">Sub TTL</td>
										<td class="ttl6"></td>
									</tr>
									<tr>
										<td colspan="2">Sistemas</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td rowspan="5">Ingeniería</td>
										<td>Taller</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Apoyo Ing</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Tableros</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Mantenimiento</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Diseño</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td colspan="2">Sub TTL</td>
										<td class="ttl7"></td>
									</tr>
									<tr>
										<td colspan="2">Calidad</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									
									<tr>
										<td colspan="2">TTL Casa Matriz</td>
										<td class="ttl8"></td>
									</tr>
									<tr>
										<td rowspan="5">Manuf</td>
										<td>Inspección</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Service Parts</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Cables</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Elect.</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td>Santa Clara</td>
										<td><input type='number' class="form-control" /></td>
									</tr>
									<tr>
										<td colspan="2">TTL Planta AGS</td>
										<td class="ttl9"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingOne">
						<h5 class="mb-0">
							<button class="btn btn-link" data-toggle="collapse" data-target="#table1" aria-expanded="false" aria-controls="table1">
								1.-Número de Personas ADMINISTRATIVOS
							</button>
						</h5>
					</div>
					<div id="table1" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body">
							<table class='table table-bordered' style='margin-left:20px;font-size: 12px;'>
								<thead>
									<tr>
										<th colspan='3'>
											ADMINISTRATIVOS
										</th>
										<th>
											ADMIN. GEN
										</th>
										<th>
											P.C.
										</th>
										<th>
											M.C.
										</th>
										<th>
											MFG.
										</th>
										<th>
											INGE.
										</th>
										<th>
											CALI.
										</th>
										<th>
											TTL
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan='3'>Casa Mtriz (*)</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab106"></td>
									</tr>
									<tr>
										<td rowspan='14'>Divi. ARNESES</td>
										<td>
											AGS
										</td>
										<td>
											ARNESES
										</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab116"></td>
									</tr>
									<tr>
										<td colspan='2'></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab126"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab130"></td>
										<td class="tab131"></td>
										<td class="tab132"></td>
										<td class="tab133"></td>
										<td class="tab134"></td>
										<td class="tab135"></td>
										<td class="tab136"></td>
									</tr>
									<tr>
										<td>MTH</td>
										<td>C&C</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab146"></td>
									</tr>
									<tr>
										<td></td>
										<td>Assy</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab156"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab160"></td>
										<td class="tab161"></td>
										<td class="tab162"></td>
										<td class="tab163"></td>
										<td class="tab164"></td>
										<td class="tab165"></td>
										<td class="tab166"></td>
									</tr>
									<tr>
										<td>CLV</td>
										<td>C&C</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab176"></td>
									</tr>
									<tr>
										<td></td>
										<td>Assy</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab186"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab190"></td>
										<td class="tab191"></td>
										<td class="tab192"></td>
										<td class="tab193"></td>
										<td class="tab194"></td>
										<td class="tab195"></td>
										<td class="tab196"></td>
									</tr>
									<tr>
										<td>SDH</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab1106"></td>
									</tr>
									<tr>
										<td>PSC</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab1116"></td>
									</tr>
									<tr>
										<td>JER</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab1126"></td>
									</tr>
									<tr>
										<td>SFP</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab1136"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab1140"></td>
										<td class="tab1141"></td>
										<td class="tab1142"></td>
										<td class="tab1143"></td>
										<td class="tab1144"></td>
										<td class="tab1145"></td>
										<td class="tab1146"></td>
									</tr>
									<tr>
										<td>Divi. CAB</td>
										<td colspan="2">CABLES</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab1156"></td>
									</tr>
									<tr>
										<td rowspan='3'>Divi. ELE</td>
										<td colspan='2'>ELECTRONICOS</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab1166"></td>
									</tr>
									<tr>
										<td></td>
										<td>EPB</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab1176"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab1180"></td>
										<td class="tab1181"></td>
										<td class="tab1182"></td>
										<td class="tab1183"></td>
										<td class="tab1184"></td>
										<td class="tab1185"></td>
										<td class="tab1186"></td>
									</tr>
									<tr>
										<td colspan="3">TTL</td>
										<td class="tab1190"></td>
										<td class="tab1191"></td>
										<td class="tab1192"></td>
										<td class="tab1193"></td>
										<td class="tab1194"></td>
										<td class="tab1195"></td>
										<td class="tab1196"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingOne">
						<h5 class="mb-0">
							<button class="btn btn-link" data-toggle="collapse" data-target="#table2" aria-expanded="false" aria-controls="table2">
								1.-Número de Personas OPERARIOS
							</button>
						</h5>
					</div>
					<div id="table2" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body">
							<table class='table table-bordered' style='margin-left:20px;font-size: 12px;'>
								<thead>
									<tr>
										<th colspan='3'>
											OPERARIOS
										</th>
										<th>
											ADMIN. GEN
										</th>
										<th>
											P.C.
										</th>
										<th>
											M.C.
										</th>
										<th>
											MFG.
										</th>
										<th>
											INGE.
										</th>
										<th>
											CALI.
										</th>
										<th>
											TTL
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan='3'>Casa Mtriz (*)</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab206"></td>
									</tr>
									<tr>
										<td rowspan='14'>Divi. ARNESES</td>
										<td>
											AGS
										</td>
										<td>
											ARNESES
										</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab216"></td>
									</tr>
									<tr>
										<td colspan='2'></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab226"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab230"></td>
										<td class="tab231"></td>
										<td class="tab232"></td>
										<td class="tab233"></td>
										<td class="tab234"></td>
										<td class="tab235"></td>
										<td class="tab236"></td>
									</tr>
									<tr>
										<td>MTH</td>
										<td>C&C</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab246"></td>
									</tr>
									<tr>
										<td></td>
										<td>Assy</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab256"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab260"></td>
										<td class="tab261"></td>
										<td class="tab262"></td>
										<td class="tab263"></td>
										<td class="tab264"></td>
										<td class="tab265"></td>
										<td class="tab266"></td>
									</tr>
									<tr>
										<td>CLV</td>
										<td>C&C</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab276"></td>
									</tr>
									<tr>
										<td></td>
										<td>Assy</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab286"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab290"></td>
										<td class="tab291"></td>
										<td class="tab292"></td>
										<td class="tab293"></td>
										<td class="tab294"></td>
										<td class="tab295"></td>
										<td class="tab296"></td>
									</tr>
									<tr>
										<td>SDH</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab2106"></td>
									</tr>
									<tr>
										<td>PSC</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab2116"></td>
									</tr>
									<tr>
										<td>JER</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab2126"></td>
									</tr>
									<tr>
										<td>SFP</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab2136"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab2140"></td>
										<td class="tab2141"></td>
										<td class="tab2142"></td>
										<td class="tab2143"></td>
										<td class="tab2144"></td>
										<td class="tab2145"></td>
										<td class="tab2146"></td>
									</tr>
									<tr>
										<td>Divi. CAB</td>
										<td colspan="2">CABLES</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab2156"></td>
									</tr>
									<tr>
										<td rowspan='3'>Divi. ELE</td>
										<td colspan='2'>ELECTRONICOS</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab2166"></td>
									</tr>
									<tr>
										<td></td>
										<td>EPB</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab2176"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab2180"></td>
										<td class="tab2181"></td>
										<td class="tab2182"></td>
										<td class="tab2183"></td>
										<td class="tab2184"></td>
										<td class="tab2185"></td>
										<td class="tab2186"></td>
									</tr>
									<tr>
										<td colspan="3">TTL</td>
										<td class="tab2190"></td>
										<td class="tab2191"></td>
										<td class="tab2192"></td>
										<td class="tab2193"></td>
										<td class="tab2194"></td>
										<td class="tab2195"></td>
										<td class="tab2196"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingOne">
						<h5 class="mb-0">
							<button class="btn btn-link" data-toggle="collapse" data-target="#table4" aria-expanded="false" aria-controls="table4">
								2.-Movimiento de Personas
							</button>
						</h5>
					</div>
					<div id="table4" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body">
							<table class='table table-bordered' style='margin-left:20px;font-size: 12px;'>
								<thead>
									<tr>
										<th colspan='3' rowspan='2'>
											
										</th>
										<th colspan='4'>
											ADMINISTRATIVOS
										</th>
										<th colspan='4'>
											OPERARIOS
										</th>
										<th>
											TTL
										</th>
									</tr>
									<tr>
									<th>
											IDM
										</th>
										<th>
											Bajas
										</th>
										<th>
											Ingreso
										</th>
										<th>
											FDM①
										</th>
										<th>
											IDM
										</th>
										<th>
											Bajas
										</th>
										<th>
											Ingreso
										</th>
										<th>
											FDM②
										</th>
										<th>
											①+②
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan='3'>Casa Mtriz (*)</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab403"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab407"></td>
										<td class="tab408"></td>
									</tr>
									<tr>
										<td rowspan='14'>Divi. ARNESES</td>
										<td>
											AGS
										</td>
										<td>
											ARNESES
										</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab413"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab417"></td>
										<td class="tab418"></td>
									</tr>
									<tr>
										<td colspan='2'></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab423"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab427"></td>
										<td class="tab428"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab430"></td>
										<td class="tab431"></td>
										<td class="tab432"></td>
										<td class="tab433"></td>
										<td class="tab434"></td>
										<td class="tab435"></td>
										<td class="tab436"></td>
										<td class="tab437"></td>
										<td class="tab438"></td>
									</tr>
									<tr>
										<td>MTH</td>
										<td>C&C</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab443"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab447"></td>
										<td class="tab448"></td>
									</tr>
									<tr>
										<td></td>
										<td>Assy</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab453"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab457"></td>
										<td class="tab458"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab460"></td>
										<td class="tab461"></td>
										<td class="tab462"></td>
										<td class="tab463"></td>
										<td class="tab464"></td>
										<td class="tab465"></td>
										<td class="tab466"></td>
										<td class="tab467"></td>
										<td class="tab468"></td>
									</tr>
									<tr>
										<td>CLV</td>
										<td>C&C</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab473"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab477"></td>
										<td class="tab478"></td>
									</tr>
									<tr>
										<td></td>
										<td>Assy</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab483"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab487"></td>
										<td class="tab488"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab490"></td>
										<td class="tab491"></td>
										<td class="tab492"></td>
										<td class="tab493"></td>
										<td class="tab494"></td>
										<td class="tab495"></td>
										<td class="tab496"></td>
										<td class="tab497"></td>
										<td class="tab498"></td>
									</tr>
									<tr>
										<td>SDH</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4103"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4107"></td>
										<td class="tab4108"></td>
									</tr>
									<tr>
										<td>PSC</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4113"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4117"></td>
										<td class="tab4118"></td>
									</tr>
									<tr>
										<td>JER</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4123"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4127"></td>
										<td class="tab4128"></td>
									</tr>
									<tr>
										<td>SFP</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4133"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4137"></td>
										<td class="tab4138"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab4140"></td>
										<td class="tab4141"></td>
										<td class="tab4142"></td>
										<td class="tab4143"></td>
										<td class="tab4144"></td>
										<td class="tab4145"></td>
										<td class="tab4146"></td>
										<td class="tab4147"></td>
										<td class="tab4148"></td>
									</tr>
									<tr>
										<td>Divi. CAB</td>
										<td colspan="2">CABLES</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4153"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4157"></td>
										<td class="tab4158"></td>
									</tr>
									<tr>
										<td rowspan='3'>Divi. ELE</td>
										<td colspan='2'>ELECTRONICOS</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4163"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4167"></td>
										<td class="tab4168"></td>
									</tr>
									<tr>
										<td></td>
										<td>EPB</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4173"></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab4177"></td>
										<td class="tab4178"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab4180"></td>
										<td class="tab4181"></td>
										<td class="tab4182"></td>
										<td class="tab4183"></td>
										<td class="tab4184"></td>
										<td class="tab4185"></td>
										<td class="tab4186"></td>
										<td class="tab4187"></td>
										<td class="tab4188"></td>
									</tr>
									<tr>
										<td colspan="3">TTL</td>
										<td class="tab4190"></td>
										<td class="tab4191"></td>
										<td class="tab4192"></td>
										<td class="tab4193"></td>
										<td class="tab4194"></td>
										<td class="tab4195"></td>
										<td class="tab4196"></td>
										<td class="tab4197"></td>
										<td class="tab4198"></td>
									</tr>
									<tr>
										<td colspan="5" rowspan="2"></td>
										<td>hombres</td>
										<td><input type='number' class="tab4200 form-control" /></td>
										<td></td>
										<td></td>
										<td></td>
										<td><input type='number' class="tab4201 form-control" /></td>
										<td class="tab4202"></td>
									</tr>
									<tr>
										<td>mujeres</td>
										<td><input type='number' class="tab4210 form-control" /></td>
										<td></td>
										<td></td>
										<td></td>
										<td><input type='number' class="tab4211 form-control" /></td>
										<td class="tab4212"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingOne">
						<h5 class="mb-0">
							<button class="btn btn-link" data-toggle="collapse" data-target="#table5" aria-expanded="false" aria-controls="table5">
								3.-Número promedio de Personas del mes
							</button>
						</h5>
					</div>
					<div id="table5" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body">
							<table class='table table-bordered' style='margin-left:20px;font-size: 12px;'>
								<thead>
									<tr>
										<th colspan='3'>
											
										</th>
										<th>
											ADM
										</th>
										<th>
											O/P
										</th>
										<th>
											TTL
										</th>
									</tr>
									
								</thead>
								<tbody>
									<tr>
										<td colspan='3'>Casa Mtriz (*)</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab502"></td>
									</tr>
									<tr>
										<td rowspan='14'>Divi. ARNESES</td>
										<td>
											AGS
										</td>
										<td>
											ARNESES
										</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab512"></td>
									</tr>
									<tr>
										<td></td>
										<td>OTROS</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab522"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab530"></td>
										<td class="tab531"></td>
										<td class="tab532"></td>
									</tr>
									<tr>
										<td>MTH</td>
										<td>C&C</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab542"></td>
									</tr>
									<tr>
										<td></td>
										<td>Assy</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab552"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab560"></td>
										<td class="tab561"></td>
										<td class="tab562"></td>
									</tr>
									<tr>
										<td>CLV</td>
										<td>C&C</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab572"></td>
									</tr>
									<tr>
										<td></td>
										<td>Assy</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab582"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab590"></td>
										<td class="tab591"></td>
										<td class="tab592"></td>
									</tr>
									<tr>
										<td>SDH</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab5102"></td>
									</tr>
									<tr>
										<td>PSC</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab5112"></td>
									</tr>
									<tr>
										<td>JER</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab5122"></td>
									</tr>
									<tr>
										<td>SFP</td>
										<td></td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab5132"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab5140"></td>
										<td class="tab5141"></td>
										<td class="tab5142"></td>
									</tr>
									<tr>
										<td>Divi. CAB</td>
										<td colspan="2">CABLES</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab5152"></td>
									</tr>
									<tr>
										<td rowspan='3'>Divi. ELE</td>
										<td colspan='2'>ELECTRONICOS</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab5162"></td>
									</tr>
									<tr>
										<td></td>
										<td>EPB</td>
										<td><input type='number' class="form-control" /></td>
										<td><input type='number' class="form-control" /></td>
										<td class="tab5172"></td>
									</tr>
									<tr>
										<td></td>
										<td>Sub TTL</td>
										<td class="tab5180"></td>
										<td class="tab5181"></td>
										<td class="tab5182"></td>
									</tr>
									<tr>
										<td colspan="3">TTL</td>
										<td class="tab5190"></td>
										<td class="tab5191"></td>
										<td class="tab5192"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
		<button class='btn btn-success btn-gpersonal'>Guardar</button>
		</div>
		<!--<div class='row'><button class='btn btn-success btn-lg btn-guardar'>Guardar</button></div>-->

		<?php
	}
	public function plantillaadmin($ano, $u){
		
	}
	public function plantillaplanta($ano,$u){
		
	}
	public function guardar(){
		$u=authDriver::getUser();

		$d = Personal::find("all", array("conditions"=>"ano='".$_POST['an']."' and mes='".$_POST['mes']."' and planta='".$u->planta."'"))[0];
		if($d){
			$d->ano=$_POST['an'];
			$d->mes=$_POST['mes'];
			$d->valores=$_POST['valores'];
			$d->user_id=$u->id;
			if($d->is_valid()) {
				$d->save();
			}
		}else{
			$d = new Personal();
			$d->ano=$_POST['an'];
			$d->mes=$_POST['mes'];
			$d->valores=$_POST['valores'];
			$d->user_id=$u->id;
			$d->planta=$u->planta;
			if($d->is_valid()) {
				$d->save();
			}
		}
	}
	public function getd(){
		$d = new Abscense();
		$d->type="adm";
		$d->fecha='2019';
		$d->valores='asdad';
		$d->user_id=1;
		if($d->is_valid()) {
			$d->save();
		}
	}
	public function validate($valor){
		return false;
	}
}