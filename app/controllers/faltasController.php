<?php
class faltasController extends controllerBase {
	public function index(){
		if( authDriver::isLoggedin() ) {
            $this->getView()->controlPanel();
        } else {
            $this->getView()->index();
        }
	}
	public function get(){
		$u = authDriver::getUser();
		$g = array();
		if($u->planta=='GLOBAL'){
			$d = Abscense::find("all", array("conditions"=>"fecha='".$_POST['an']."' and planta<>'".$u->planta."'"));
			foreach($d as $obj){				
				$ob=json_decode($obj->valores);
				for($i=0;$i<count($ob);$i++){
					if(!isset($g[$i])){
						$g[$i]=0;
					}
					if(!isset($ob[$i])){
						$vl=0;
					}else{
						$vl=floatval($ob[$i]);
					}

					$g[$i]+=$vl;
				}
			}
			if($g){
				echo json_encode($g);
			}else{
				echo json_encode(0);
			}
		}else{
			$d = Abscense::find("all", array("conditions"=>"fecha='".$_POST['an']."' and planta='".$u->planta."'"))[0];
			if($d){
				echo json_encode(json_decode($d->valores));
			}else{
				echo json_encode(0);
			}
		}
		
	}	
	public function plantilla($ano){
		$u = authDriver::getUser();
		echo $ano;
		if($u->planta!='GLOBAL'){
			echo "<div class='row'>";

		}else{
			echo "<div class='row gbal'>";
		}
		?>
			<table class='table table-bordered' style='margin-left:20px;font-size: 12px;'>
				<thead></thead>
				<tbody>
					<tr>
						<td colspan='4' rowspan='2'>
						
						</td>
						<td colspan='2'>
							ENERO
						</td>
						<td colspan='2'>
							FEBRERO
						</td>
						<td colspan='2'>
							MARZO
						</td>
						<td colspan='2'>
							ABRIL
						</td>
						<td colspan='2'>
							MAYO
						</td>
						<td colspan='2'>
							JUNIO
						</td>
						<td colspan='2'>
							JULIO
						</td>
						<td colspan='2'>
							AGOSTO
						</td>
						<td colspan='2'>
							SEPTIEMBRE
						</td>
						<td colspan='2'>
							OCTUBRE
						</td>
						<td colspan='2'>
							NOVIEMBRE
						</td>
						<td colspan='2'>
							DICIEMBRE
						</td>
					</tr>
					<tr>
						<td>Número</td>
						<td>%</td>
						<td>Número</td>
						<td>%</td>
						<td>Número</td>
						<td>%</td>
						<td>Número</td>
						<td>%</td>
						<td>Número</td>
						<td>%</td>
						<td>Número</td>
						<td>%</td>
						<td>Número</td>
						<td>%</td>
						<td>Número</td>
						<td>%</td>
						<td>Número</td>
						<td>%</td>
						<td>Número</td>
						<td>%</td>
						<td>Número</td>
						<td>%</td>
						<td>Número</td>
						<td>%</td>
					</tr>
					<tr class='iniciomesadm'>
						<td rowspan='4'>
							INICIO DE MES
						</td>
						<td rowspan='4'>
							
						</td>
						<td rowspan='3'>
							Distribución de empleados
						</td>
						<td>
							ADMINISTRATIVOS
						</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='iniciomesindi'>
						<td>
							INDIRECTOS
						</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='iniciomesdir'>
						<td>
							DIRECTOS
						</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='iniciodemestotal'>
						<td colspan='2'>TOTAL</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class='tadmcontratoplanta'>
						<td rowspan='14'>CIERRE DE MES</td>
						<td rowspan='7'>TIPOS DE CONTRATO</td>
						<td rowspan='2'>ADMINISTRATIVOS</td>
						<td>PLANTA</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='tadmcontratoprueba'>
						<td>PRUEBA</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr  class='tadmcontratosubtotal'>
						<td colspan='2'>SUBTOTAL</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class='topecontratoplanta'>
						<td rowspan='2'>OPERARIOS</td>
						<td>PLANTA</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='topecontratoprueba'>
						<td>PRUEBA</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='topecontratosubtotal'>
						<td colspan='2'>SUBTOTAL</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class='topecontratostotal'>
						<td colspan='2'>
							TOTAL
						</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class='sexoadmplanta'>
						<td rowspan='7'>SEXO</td>
						<td rowspan='2'>ADMINISTRATIVOS</td>
						<td>HOMBRES</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='sexoadmprueba'>
						<td>MUJERES</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='sexoadmsubtotal'>
						<td colspan='2'>SUBTOTAL</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class='sexoopeplanta'>
						<td rowspan='2'>OPERARIOS</td>
						<td>HOMBRES</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='sexoopeprueba'>
						<td>MUJERES</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='sexoopesubtotal'>
						<td colspan='2'>SUBTOTAL</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class='sexototal'>
						<td colspan='2'>
							TOTAL
						</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class='altasadm'>
						<td rowspan='10'>
							MOVIMIENTOS DE PERSONAL
						</td>
						<td rowspan='3'>ALTAS</td>
						<td rowspan='2'>NUEVO PERSONAL(PRUEBA)</td>
						<td>ADMINISTRATIVOS</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='altasope'>
						<td>OPERARIOS</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='altastotal'>
						<td colspan='2'>TOTAL</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class='bajasadmplanta'>
						<td rowspan='7'>BAJAS</td>
						<td rowspan='2'>ADMINISTRATIVOS</td>
						<td>PLANTA</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='bajasadmprueba'>
						<td>PRUEBA</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='bajasadmsubtotal'>
						<td colspan='2'>SUBTOTAL</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class='bajasopeplanta'>
						<td rowspan='2'>OPERARIOS</td>
						<td>PLANTA</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='bajasopeprueba'>
						<td>PRUEBA</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class="bajasopesubtotal">
						<td colspan='2'>SUBTOTAL</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class="bajastotal">
						<td colspan='2'>TOTAL</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class="finadm">
						<td rowspan=3>FIN DE MES</td>
						<td rowspan=3></td>
						<td colspan=2>TOTAL ADMINISTRATIVOS EN EL MES</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class="finope">
						<td colspan=2>TOTAL OPERARIO EN EL MES</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class='findemestotal'>
						<td colspan=2>TOTAL de PERSONAS EN EL MES</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>

					<tr>
						<td colspan=4>TIPO de AUSENTISMO</td>
						<td colspan=2>Total de Dias</td>
						<td colspan=2>Total de Dias</td>
						<td colspan=2>Total de Dias</td>
						<td colspan=2>Total de Dias</td>
						<td colspan=2>Total de Dias</td>
						<td colspan=2>Total de Dias</td>
						<td colspan=2>Total de Dias</td>
						<td colspan=2>Total de Dias</td>
						<td colspan=2>Total de Dias</td>
						<td colspan=2>Total de Dias</td>
						<td colspan=2>Total de Dias</td>
						<td colspan=2>Total de Dias</td>
					</tr>
					<tr class='peradminc'>
						<td rowspan='9' colspan=2>AUSENTISMO DEL PERIODO</td>
						<td rowspan='3'>ADMINISTRATIVO</td>
						<td>INCAPACIDAD</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='peradmvac'>
						<td>VACACIONES</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='peradmfal'>
						<td>FALTAS INJUSTIFICADAS</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='peradmsubtotal'>
						<td colspan=2>SUBTOTAL</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class='peropeinc'>
						<td rowspan=3>OPERARIOS</td>
						<td>INCAPACIDAD</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='peropevac'>
						<td>VACACIONES</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='peropefal'>
						<td>FALTAS INJUSTIFICADAS</td>
						<td><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td class='por1'></td>
						<td><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td class='por2'></td>
						<td><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td class='por3'></td>
						<td><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td class='por4'></td>
						<td><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td class='por5'></td>
						<td><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td class='por6'></td>
						<td><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td class='por7'></td>
						<td><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td class='por8'></td>
						<td><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td class='por9'></td>
						<td><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td class='por10'></td>
						<td><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td class='por11'></td>
						<td><input type='number' data-mes='12' class='form-control mes12' /></td>
						<td class='por12'></td>
					</tr>
					<tr class='peropesubtotal'>
						<td colspan=2>SUBTOTAL</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class='pertotal'>
						<td colspan=2>TOTAL</td>
						<td class='inp1'></td>
						<td class='por1'></td>
						<td class='inp2'></td>
						<td class='por2'></td>
						<td class='inp3'></td>
						<td class='por3'></td>
						<td class='inp4'></td>
						<td class='por4'></td>
						<td class='inp5'></td>
						<td class='por5'></td>
						<td class='inp6'></td>
						<td class='por6'></td>
						<td class='inp7'></td>
						<td class='por7'></td>
						<td class='inp8'></td>
						<td class='por8'></td>
						<td class='inp9'></td>
						<td class='por9'></td>
						<td class='inp10'></td>
						<td class='por10'></td>
						<td class='inp11'></td>
						<td class='por11'></td>
						<td class='inp12'></td>
						<td class='por12'></td>
					</tr>
					<tr class='diaslab'>
						<td colspan=4>Dias laborados en el mes</td>
						<td colspan=2><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td colspan=2><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td colspan=2><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td colspan=2><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td colspan=2><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td colspan=2><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td colspan=2><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td colspan=2><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td colspan=2><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td colspan=2><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td colspan=2><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td colspan=2><input type='number' data-mes='12' class='form-control mes12' /></td>
					</tr>

					<tr>
						<td colspan=2 rowspan=3>AUSENTISMO Y ROTACION</td>
						<td colspan=2>Promedio de trabajadores</td>
						<td colspan=2><input type='number' data-mes='1' class='form-control mes1' /></td>
						<td colspan=2><input type='number' data-mes='2' class='form-control mes2' /></td>
						<td colspan=2><input type='number' data-mes='3' class='form-control mes3' /></td>
						<td colspan=2><input type='number' data-mes='4' class='form-control mes4' /></td>
						<td colspan=2><input type='number' data-mes='5' class='form-control mes5' /></td>
						<td colspan=2><input type='number' data-mes='6' class='form-control mes6' /></td>
						<td colspan=2><input type='number' data-mes='7' class='form-control mes7' /></td>
						<td colspan=2><input type='number' data-mes='8' class='form-control mes8' /></td>
						<td colspan=2><input type='number' data-mes='9' class='form-control mes9' /></td>
						<td colspan=2><input type='number' data-mes='10' class='form-control mes10' /></td>
						<td colspan=2><input type='number' data-mes='11' class='form-control mes11' /></td>
						<td colspan=2><input type='number' data-mes='12' class='form-control mes12' /></td>
					</tr>

					<tr class='ausrot'>
						<td colspan=2>ROTACION</td>
						<td colspan=2 class='inp1'></td>
						<td colspan=2 class='inp2'></td>
						<td colspan=2 class='inp3'></td>
						<td colspan=2 class='inp4'></td>
						<td colspan=2 class='inp5'></td>
						<td colspan=2 class='inp6'></td>
						<td colspan=2 class='inp7'></td>
						<td colspan=2 class='inp8'></td>
						<td colspan=2 class='inp9'></td>
						<td colspan=2 class='inp10'></td>
						<td colspan=2 class='inp11'></td>
						<td colspan=2 class='inp12'></td>
					</tr>
					<tr class='rot' >
						<td colspan=2>AUSENTISMO</td>
						<td colspan=2 class='inp1'></td>
						<td colspan=2 class='inp2'></td>
						<td colspan=2 class='inp3'></td>
						<td colspan=2 class='inp4'></td>
						<td colspan=2 class='inp5'></td>
						<td colspan=2 class='inp6'></td>
						<td colspan=2 class='inp7'></td>
						<td colspan=2 class='inp8'></td>
						<td colspan=2 class='inp9'></td>
						<td colspan=2 class='inp10'></td>
						<td colspan=2 class='inp11'></td>
						<td colspan=2 class='inp12'></td>
					</tr>


					<tr>
						<td colspan=2 rowspan=2>INCAPACIDAD POR MATERNIDAD</td>
						<td colspan=2>POST NATAL</td>
						<!--<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>-->
					</tr>

					<tr>
						<td colspan=2>PRE NATAL</td>
						<!--<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>
						<td><input type='number' class='form-control' /></td>
						<td></td>-->
					</tr>
				</tbody>
			</table>
		</div>
		<?php
		if($u->planta!='GLOBAL'){
			?>
		<br /><br /><button class='btn btn-success btn-lg btn-guardar'>Guardar</button>

		<?php
		}
	}
	public function plantillaadmin($ano, $u){
		
	}
	public function plantillaplanta($ano,$u){
		
	}
	public function guardar(){
		$u=authDriver::getUser();

		$d = Abscense::find("all", array("conditions"=>"fecha='".$_POST['an']."' and planta='".$u->planta."'"))[0];
		if($d){
			$d->fecha=$_POST['an'];
			$d->valores=$_POST['valores'];
			$d->user_id=$u->id;
			if($d->is_valid()) {
				$d->save();
			}
		}else{
			$d = new Abscense();
			$d->fecha=$_POST['an'];
			$d->valores=$_POST['valores'];
			$d->user_id=$u->id;
			$d->planta=$u->planta;
			if($d->is_valid()) {
				$d->save();
			}
		}
	}
	public function getd(){
		$d = new Abscense();
		$d->type="adm";
		$d->fecha='2019';
		$d->valores='asdad';
		$d->user_id=1;
		if($d->is_valid()) {
			$d->save();
		}
	}
	public function validate($valor){
		return false;
	}
}