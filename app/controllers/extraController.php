<?php
class extraController extends controllerBase {
	public function index(){
		
	}
	public function plantilla($ano){
		$u=authDriver::getUser();
		$this->plantillaplanta($ano, $u);
	}
	public function plantillaadmin($ano, $u){
		
	}
	public function plantillaplanta($ano,$u){
		$txt = "";
		$inp = "";
		$j = true;
		$t = 0;
		$arr = array();
		$arra = array();
		$arra[0] = array("nombre" => "ADMINISTRATIVO", "clase" => "adm");
		$arra[1] = array("nombre" => "OPERATIVO", "clase" => "ope");
		$arra[2] = array("nombre" => "OPERATIVO TIEMPO EXTRA", "clase" => "opex");
		$adm =  (array) json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='adm' and planta='".$u->planta."'"))[0]->valores);
		$ope =  (array)  json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='ope' and planta='".$u->planta."'"))[0]->valores);
		$opex =  (array)  json_decode(Extratime::find("all", array("conditions"=>"fecha='".$ano."' and type='opex' and planta='".$u->planta."'"))[0]->valores);
		?>
		<div class="row" style="width:100%">
			<div id="accordion" style="width:100%">
		<?php
		$c=0;
		foreach($arra as $a){
			?>
			<div class="card">
				<div class="card-header" id="headingOne">
					<h5 class="mb-0">
						<button class="btn btn-link" data-toggle="collapse" data-target="#table<?php echo $c;?>" aria-expanded="false" aria-controls="table<?php echo $c;?>">
						<?php echo $a["nombre"]; ?>
						</button>
					</h5>
				</div>
				<div id="table<?php echo $c;?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
					<div class="card-body">

			<?php
			echo "<table class='table table-hover'>
					<tr>
						<td></td>";
			for($i = 1; $i <= 31; $i++){
				echo "<td>" . $i . "</td>";
			}
			echo "</tr>";
			for($i = 1; $i <= 12; $i++){
				echo "<tr>";
				$d = cal_days_in_month(CAL_GREGORIAN, $i, $ano);
				$da = date("M",strtotime("2020-".$i."-01"));
				echo "<td>".$da."</td>";
				for($j = 1; $j <= $d; $j++){
					
					if($a["clase"]=='adm'){
						$vale = $adm[$i."-".$j];

					}else if($a["clase"]=='ope'){
						$vale = $ope[$i."-".$j];

					}else if($a["clase"]=='opex'){
						$vale = $opex[$i."-".$j];

					}
					if($vale == "" ){
						$vale = '0';
					}
					
					echo "<td><input type='number' style='padding-left:0px;padding-right:0px;' min='0' class='form-control ".$a["clase"]."' name='".$i."-".$j."' value='".$vale."' /></td>";
				}
				echo "</tr>";
			}
			echo "</table>";
			echo "</div>
			</div>
		</div>";
		$c++;
		}
		
		/*foreach($arra as $a){
			echo "<div class='row'>";
			echo "<div class='row'><h3>".$a["nombre"]."</h3></div>";
			for($i = 1; $i <= $d; $i++){
				echo "<div class='col-xs-1 no-padding'>
					<div class='col-xs-12'>" . $i . "</div>
					<div class='col-xs-12 no-padding'><input class='form-control inp' name='".$a["clase"].$i."' value='0' /></div>
				</div>";
			}
			echo "</div>";
		}*/
		echo "</div>
		</div>
		<br /><br /><button class='btn btn-success btn-lg btn-guardar'>Guardar</button>";
	}
	public function guardar(){
		$u=authDriver::getUser();

		$d =  Extratime::find("all", array("conditions"=>"fecha='".$_POST['an']."' and type='adm' and planta='".$u->planta."'"))[0];
		if($d){
			$d->type="adm";
			$d->fecha=$_POST['an'];
			$d->valores=$_POST['adm'];
			$d->user_id=$u->id;
			if($d->is_valid()) {
				$d->save();
			}
		}else{
			$d = new Extratime();
			$d->type="adm";
			$d->fecha=$_POST['an'];
			$d->valores=$_POST['adm'];
			$d->user_id=$u->id;
			$d->planta=$u->planta;
			if($d->is_valid()) {
				$d->save();
			}
		}
		
		$d =  Extratime::find("all", array("conditions"=>"fecha='".$_POST['an']."' and type='ope' and planta='".$u->planta."'"))[0];
		if($d){
			$d->type="ope";
			$d->fecha=$_POST['an'];
			$d->valores=$_POST['ope'];
			$d->user_id=$u->id;
			if($d->is_valid()) {
				$d->save();
			}
		}else{
			$d = new Extratime();
			$d->type="ope";
			$d->fecha=$_POST['an'];
			$d->valores=$_POST['ope'];
			$d->user_id=$u->id;
			$d->planta=$u->planta;
			if($d->is_valid()) {
				$d->save();
			}
		}

		$d =  Extratime::find("all", array("conditions"=>"fecha='".$_POST['an']."' and type='opex' and planta='".$u->planta."'"))[0];
		if($d){
			$d->type="opex";
			$d->fecha=$_POST['an'];
			$d->valores=$_POST['opex'];
			$d->user_id=$u->id;
			$d->planta=$u->planta;
			if($d->is_valid()) {
				$d->save();
			}
		}else{
			$d = new Extratime();
			$d->type="opex";
			$d->fecha=$_POST['an'];
			$d->valores=$_POST['opex'];
			$d->user_id=$u->id;
			if($d->is_valid()) {
				$d->save();
			}
		}
		
	}
	public function getd(){
		$d = Extratime::find('all');
		print_r($d);
	}
	public function validate($valor){
		return false;
	}
}