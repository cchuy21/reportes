
<?php

	class paypalDriver extends driverBase {
		protected $_credentials = array(
	      'USER' => 'sgr_81-facilitator_api1.hotmail.com',
	      'PWD' => '6DNDXDE7BKYZ2M95',
	      'SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31Aq0h7mhwSX.ROhqln3gMYPgLoKyi',
	   );
	   protected $_endPoint = 'https://api-3t.sandbox.paypal.com/nvp';
	   protected $_version = '74.0';
		public function request($method,$params = array()) {
		      $this -> _errors = array();
		      if( empty($method) ) { //Check if API method is not empty
		         $this -> _errors = array('API method is missing');
		         return false;
		      }
		
		      //Our request parameters
		      $requestParams = array(
		         'METHOD' => $method,
		         'VERSION' => $this -> _version
		      ) + $this -> _credentials;
		
		      //Building our NVP string
		      $request = http_build_query($requestParams + $params);
		
		      //cURL settings
		      $curlOptions = array (
		         CURLOPT_URL => $this -> _endPoint,
		         CURLOPT_VERBOSE => 1,
		         CURLOPT_SSL_VERIFYPEER => true,
		         CURLOPT_SSL_VERIFYHOST => 2,
		         CURLOPT_CAINFO => dirname(__FILE__) . '/cacert.pem', //CA cert file
		         CURLOPT_RETURNTRANSFER => 1,
		         CURLOPT_POST => 1,
		         CURLOPT_POSTFIELDS => $request
		      );
		
		      $ch = curl_init();
		      curl_setopt_array($ch,$curlOptions);
		
		      //Sending our request - $response will hold the API response
		      $response = curl_exec($ch);
		
		      //Checking for cURL errors
		      if (curl_errno($ch)) {
		         $this -> _errors = curl_error($ch);
		         curl_close($ch);
		         return false;
		         //Handle errors
		      } else  {
		         curl_close($ch);
		         $responseArray = array();
		         parse_str($response,$responseArray); // Break the NVP string to an array
		         return $responseArray;
		      }
	   }
   }
