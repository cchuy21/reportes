<?php
class pdfDriver extends driverBase {
	//recibe los parametros que va a tomar en cuenta el formato del pdf
	public static function doPDF($path='',$content='',$body=false,$style='',$mode=false,$paper_1='P',$paper_2='80',$alto='330') 
{

    if( $body!=true and $body!=false ) $body=false;
    if( $mode!=true and $mode!=false ) $mode=false;

    if( $body == true )
    {
        $content='
        <!doctype html>
        <html>
        <head>
            <link rel="stylesheet" href="'.$style.'" type="text/css" />
        </head>
        <body>'
            .$content.
        '</body>
        </html>';
    }

    if( $content!='' )
    {
    	 ob_end_clean(); //limpia el contenido 
        ob_start(); //activa el almacenamiento en el bufer
        if(!strstr($content, '<page>'))
        	echo $content;
		else{
			echo $content;
		}
		//echo '<page>'.$content.'</page>';
		
        //Añadimos la extensión del archivo. Si está vacío el nombre lo creamos
        $path!='' ? $path .='.pdf' : $path = crearNombre(10);  
            
        $content = ob_get_clean(); //obtiene el contenido del bufer y elimina lo que hay en el
        require_once('static/html2pdf2/html2pdf.class.php'); //archivo que requiere
         
        //Orientación / formato del pdf y el idioma.
        $pdf = new HTML2PDF('',array(80,$alto),'es',true,'ISO-8859-15', array(2, 1, 1, 1)); //los márgenes también pueden definirse en <page> ver documentación
        
        $pdf->WriteHTML($content); //se escribe el archivo en una página web
        
        if($mode==false)
        {
            //El pdf es creado "al vuelo", el nombre del archivo aparecerá predeterminado cuando le demos a guardar
            $pdf->Output($path); // mostrar
            //$pdf->Output('ejemplo.pdf', 'D');  //forzar descarga 
        }
        else
        {
            $pdf->Output($path, 'F'); //guardar archivo ( ¡ojo! si ya existe lo sobreescribe )
            header('Location: '.$path); // abrir
        }
   
	ob_end_flush(); //enviar los datos al bufer de salida
    }

}
public static function doPDFG($path='',$content='',$body=false,$style='',$mode=false,$paper_1='L',$paper_2='letter')
{    
    if( $body!=true and $body!=false ) $body=false;
    if( $mode!=true and $mode!=false ) $mode=false;

    if( $body == true )
    {
        $content='
        <!doctype html>
        <html>
        <head>
            <link rel="stylesheet" href="'.$style.'" type="text/css" />
        </head>
        <body>'
            .$content.
        '</body>
        </html>';
    }

    if( $content!='' )
    {
        ob_start();
        
        echo '<page>'.$content.'</page>';
        
        //Añadimos la extensión del archivo. Si está vacío el nombre lo creamos
        $path!='' ? $path .='.pdf' : $path = crearNombre(10);  
            
        $content = ob_get_clean(); 
        require_once('static/html2pdf2/html2pdf.class.php'); 
        
        //Orientación / formato del pdf y el idioma.
        $pdf = new HTML2PDF($paper_1,$paper_2,'es'/*, array(10, 10, 10, 10) /*márgenes*/); //los márgenes también pueden definirse en <page> ver documentación
        
        $pdf->WriteHTML($content);
        
        if($mode==false)
        {
            //El pdf es creado "al vuelo", el nombre del archivo aparecerá predeterminado cuando le demos a guardar
            $pdf->Output($path); // mostrar
            //$pdf->Output('ejemplo.pdf', 'D');  //forzar descarga 
        }
        else
        {
            $pdf->Output($path, 'F'); //guardar archivo ( ¡ojo! si ya existe lo sobreescribe )
            header('Location: '.$path); // abrir
        }
    
    }

}
}
