<?php

class configDriver extends driverBase
{
	static private $instance = null;
	static private $dbtype = "mysql";
    static private $controller = "systemuser";
	static private $content = "systemuser.login";
	//acceso a la base de datos
	static private $database = array(
		"mysql" => array(
			"host" => "localhost",
			"username" => "root",
			"password" => "root",
			"database" => "reportes"
		)
	);
	
	static private $mail = array("method" => "mail", "data" => array());

	public static function getDBConfig()
	{
		$dbObj = new stdClass();
		$dbObj->type = self::$dbtype;

		foreach (self::$database[self::$dbtype] as $key => $value) {
			$dbObj->$key = $value;
		}
		
		return $dbObj;
	}
	
	public static function getAutoloaders()
	{
		return self::$autoloaders;
	}
	
	public static function getMailConfig()
	{
		return self::$mail;
	}
	//no se permite realizar una copia de un usuario
	public function __clone()
	{
		trigger_error('Clone no se permite.', E_USER_ERROR);
	}

        public static function defaultController(){
		return self::$controller;
	}
        
	public static function defaultContent(){
		return self::$content;
	}
        
        public static function setDefaultView($view = null){
                if( !$view ) return false;
		self::$view = $view;
	}
	
}
